<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grouping extends Model
{
    protected $table = 'grouping';
    protected $fillable = [
    					'names'
						];
}
