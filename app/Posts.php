<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $table = 'posts';
    protected $fillable = ['title', 'category_id', 'content', 'video', 'is_video', 'views', 'admin_id', 'tgroup'];

    public static function getYoutubeId($value='')
    {
    	parse_str( parse_url($value, PHP_URL_QUERY ), $my_array_of_vars );
    	return $my_array_of_vars['v'];
    }

    public static function getSearchText($string='', $query='')
    {
    	$searchlocation = $query;

		$sentences = explode('.', $string);
		$matched = array();
		foreach($sentences as $sentence){
		    $offset = stripos($sentence, $searchlocation);
		    if($offset){ $matched[] = $sentence; }
		}

		$result = '';
    	foreach ($matched as $key => $value) {
    		if ($key >= 1) { continue; }
            // $result .= '<p>'.$value.'</p>';
    		$result .= '<p>'.strip_tags($value).'</p>';
    	}
    	return $result;
    }
}
