<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Proyek as Proyek;
use App\Admin as Admin;

class Member extends Model
{
    protected $table = 'users';
    protected $fillable = ['name', 'email', 'password', 'division', 'nik', 'proyek_id','role_member'];

    public static function proyekName($id)
    {
        $model = Proyek::where('id', $id)->get()->first();
        return $model['nama'];
    }

    public static function spvName($id)
    {
        $model = Admin::where('id_admin', $id)->get()->first();
        return $model['username'];
    }
}
