<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListAktivitas extends Model
{
    
    protected $table = 'activity_logs';
    protected $fillable = [ 'user_id', 'post_id' ];

}
