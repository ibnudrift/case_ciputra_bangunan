<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mcase extends Model
{
    protected $table = 'list_case';
    protected $fillable = [
    					'proyek_id',
    					'divisi_id',
                        'users_id',
    					'nama',
                        
                        'item_pk_id',
    					'no_case',

                        'kawasan',
                        'blok_kavling',
                        'blok_v',
                        'kavling_v',
                        'tipe_rumah',

    					'foto_sebelum',
    					'foto_sesudah',
                        
    					'content',
    					'penyebab',
    					'cara_perbaikan',
    					'status',
    					'notes',

    					'date_input',
                        'date_selesai',
                        'date_reschedule',
                        'is_archive',
						];

}
