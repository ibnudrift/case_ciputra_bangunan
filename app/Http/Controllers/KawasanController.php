<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Kawasan;
use App\Proyek;
use Auth;

class KawasanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index() {
        $kawasan = array();

        if ( isset($_GET['proyek']) ) {
            $filters_id = $_GET['proyek'];
            $kawasan = Kawasan::where('proyek_id', intval($filters_id))
            ->orderBy('id', 'desc')
            ->paginate(30);
        }

        $filter = '';
        $m_proyek = Proyek::all();

        if (isset($_GET['proyek'])) {
            $filters_id = $_GET['proyek'];
            $f_proyek = Proyek::where('id', intval($filters_id) )->first();
            $filter = $f_proyek;
        }

        return view('kawasan.index', ['kawasan' => $kawasan, 'q'=>array(), 'filter_proyek'=> $filter, 'm_proyek'=> $m_proyek]);
    }

    // public function searching(Request $request) {
    //     $q = htmlspecialchars($request->q, ENT_QUOTES);

    //     $kawasan = Kawasan::orderBy('id', 'desc')
    //                 ->where('title','LIKE','%'.$q.'%')
    //                 ->paginate(10);
    //     return view('kawasan.index', ['kawasan' => $kawasan, 'q'=> $q]);
    // }
 
    public function create() {
        $proyek = 0;
        $filter = null;
        if (isset($_GET['proyek'])) {
            $proyek = intval($_GET['proyek']);
            $f_proyek = Proyek::where('id', intval($proyek) )->first();
            $filter = $f_proyek;
        }
        return view('kawasan.create', ['proyek'=> $proyek, 'filter_proyek'=> $filter]);
    }
 
    public function edit($id) {
        $kawasan = Kawasan::findOrFail($id);
        if (isset($kawasan->proyek_id)) {
            $proyek = intval($kawasan->proyek_id);
            $f_proyek = Proyek::where('id', intval($proyek) )->first();
            $filter = $f_proyek;
        }
        return view('kawasan.edit', ['kawasan' => $kawasan, 'proyek'=> $proyek, 'filter_proyek'=> $filter]);
    }
 
    public function store(Request $request) {
        Kawasan::create($request->all());
        \Session::flash('notifikasi', 'Data berhasil ditambah.');

        return \Redirect::route('admin.searchkawasan', ['proyek'=> $request->proyek_id]);
    }
 
    public function update(Request $request, $id) {
        $kawasan = Kawasan::findOrFail($id);
        $kawasan->update($request->all());
        \Session::flash('notifikasi', 'Data berhasil diubah.');
        return \Redirect::route('admin.searchkawasan', ['proyek'=> $kawasan->proyek_id]);
    }
 
    public function destroy($id) {
        $in_proyek = 0;
        $kawasan = Kawasan::findOrFail($id);
        $in_proyek = $kawasan->proyek_id;
        Kawasan::destroy($id);
        \Session::flash('notifikasi', 'Data berhasil dihapus.');
        return \Redirect::route('admin.searchkawasan', ['proyek'=> $in_proyek]);
    }

    public function show($id) {

    }

    
}
