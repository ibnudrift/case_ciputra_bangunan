<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Karyawan;

class KaryawanController extends Controller
{
    public function index() {
        $karyawan = Karyawan::all();
        return view('karyawan.index', compact('karyawan'));
    }
 
    public function create() {
        return view('karyawan.create');
    }
 
    public function edit($id) {
        $karyawan = Karyawan::findOrFail($id);
        return view('karyawan.edit', compact('karyawan'));
    }
 
    public function store(Request $request) {
        Karyawan::create($request->all());
        \Session::flash('notifikasi', 'Karyawan berhasil ditambah.');
        return redirect('karyawan');
    }
 
    public function update(Request $request, $id) {
        $karyawan = Karyawan::findOrFail($id);
        $karyawan->update($request->all());
        \Session::flash('notifikasi', 'Karyawan berhasil diubah.');
        return redirect('karyawan');
    }
 
    public function destroy($id) {
        Karyawan::destroy($id);
        \Session::flash('notifikasi', 'Karyawan berhasil dihapus.');
        return redirect('karyawan');
    }
}
