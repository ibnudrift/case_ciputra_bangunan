<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use App\Mcase;
use App\Proyek;
use App\Divisi;
use App\Subjects;
use App\Kawasan;
use App\Blok;
use App\Tipes;
use App\Member;
use App\Admin;
use Auth;

class CasesController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index() {

        $results_tsubj = array();
        if (isset($_GET['q']) AND $_GET['q'] != '') {
            $q = trim( htmlspecialchars($_GET['q'], ENT_QUOTES) );

            $subjects_mod = Subjects::orderBy('id', 'desc')
                        ->where('title','LIKE','%'.$q.'%')
                        ->get();

            $lists_id_ar = array();
            foreach ($subjects_mod as $key => $value) {
                $lists_id_ar[] = $value->id;
            }
            $results_tsubj = implode(',', $lists_id_ar);
        }

        $filter_p = array();
        $filters_divisi = array();
        $filter_w_proyek = array();

        $m_proyek = Proyek::all();
        $admin = Admin::where('role_type', '!=', 'manajer_teknik')->get();

        if (isset($_GET['spv_id']) AND $_GET['spv_id'] != '') {
            $filters_id = $_GET['spv_id'];
            $f_proyek = Admin::where('id_admin', intval($filters_id) )->first();
            $filter_p = $f_proyek;
            $filter_w_proyek = ['list_case.users_id', $filter_p->id_admin];
            // dd($filter_w_proyek); exit;
        }

        if (isset($_GET['divisi']) AND $_GET['divisi'] != '') {
            $n_id_divisi = $_GET['divisi'];
            $filters_divisi = Member::where('id', intval($n_id_divisi) )->first();
            $filter_w_proyek = ['list_case.divisi_id', $filters_divisi->id];
        }
        
        $whereData = [
                     ['list_case.is_archive', 0],
                     $filter_w_proyek,
                     ];
        $whereData = array_filter($whereData);
        // echo "<pre>"; print_r($results_tsubj); exit;

        if ( isset($results_tsubj) AND $results_tsubj != null ) {
            $results_tsubj2 = $results_tsubj;
            $results_tsubj = array_filter( explode(',', $results_tsubj2) );

            $cases = Mcase::leftJoin('proyek', 'list_case.proyek_id', '=', 'proyek.id')
                // ->leftJoin('division', 'list_case.divisi_id', '=', 'division.id')
                ->leftJoin('users', 'list_case.divisi_id', '=', 'users.id')
                ->leftJoin('subjects', 'list_case.item_pk_id', '=', 'subjects.id')
                ->select( DB::raw('list_case.id, list_case.nama, list_case.no_case, proyek.nama as nama_proyek, users.name as nama_divisi, subjects.title as nama_item, list_case.status, list_case.foto_sesudah,
                    (CASE WHEN (`status` = 2) THEN "d_selesai"
                    WHEN ( (date_selesai - CURDATE()) <= 2 AND (date_selesai - CURDATE()) >= 1 ) THEN "d_warning"
                    WHEN (date_selesai <= CURDATE()) THEN "d_warning2"
                    WHEN (date_selesai < CURDATE()) THEN "d_terlambat"
                    ELSE "d_progress" END)
                     as status_data, list_case.blok_kavling') )
                ->whereIn('list_case.item_pk_id', $results_tsubj)
                ->where($whereData)
                ->orderBy('list_case.blok_kavling', 'asc')
                ->paginate(15);
                if (isset($_GET['proyek']) AND $_GET['proyek'] != '') {
                    $cases->appends(['proyek' => $_GET['proyek'], 'q' => isset($_GET['q'])? $_GET['q']: ''  ]);
                }
        }else{
            $cases = Mcase::leftJoin('proyek', 'list_case.proyek_id', '=', 'proyek.id')
                // ->leftJoin('division', 'list_case.divisi_id', '=', 'division.id')
                ->leftJoin('users', 'list_case.divisi_id', '=', 'users.id')
                ->leftJoin('subjects', 'list_case.item_pk_id', '=', 'subjects.id')
                // WHEN (date_selesai > CURDATE() ) THEN "d_progress"
                ->select( DB::raw('list_case.id, list_case.nama, list_case.no_case, proyek.nama as nama_proyek, users.name as nama_divisi, subjects.title as nama_item, list_case.foto_sesudah, list_case.status,
                    (CASE WHEN (`status` = 2) THEN "d_selesai"
                    WHEN ( (date_selesai - CURDATE()) <= 2 AND (date_selesai - CURDATE()) >= 1 ) THEN "d_warning"
                    WHEN (date_selesai <= CURDATE()) THEN "d_warning2"
                    WHEN (date_selesai < CURDATE()) THEN "d_terlambat"
                    ELSE "d_progress" END)
                     as status_data, list_case.blok_kavling') )
                ->where($whereData)
                ->orderBy('list_case.blok_kavling', 'asc')
                // ->whereIn('list_case.id', [10, 11, 12])
                ->paginate(15);

                if (isset($_GET['proyek']) AND $_GET['proyek'] != '') {
                    $cases->appends(['proyek' => $_GET['proyek'], 'q' => isset($_GET['q'])? $_GET['q']: ''  ]);
                }
        }
        // dd($cases);
        
        return view('case.index', ['cases' => $cases, 'q'=>isset($_GET['q'])? $_GET['q']: '', 'm_proyek'=> $m_proyek,'admins'=> $admin, 'filter_proyek'=>$filter_p, 'filters_divisi'=> $filters_divisi]);
    }

    public function arsips()
    {
        $cases = Mcase::leftJoin('proyek', 'list_case.proyek_id', '=', 'proyek.id')
                // ->leftJoin('division', 'list_case.divisi_id', '=', 'division.id')
                ->leftJoin('users', 'list_case.divisi_id', '=', 'users.id')
                ->leftJoin('subjects', 'list_case.item_pk_id', '=', 'subjects.id')
                ->orderBy('list_case.id', 'desc')
                ->select( DB::raw('list_case.id, list_case.nama, list_case.no_case, proyek.nama as nama_proyek, users.name as nama_divisi, subjects.title as nama_item, list_case.status,
                    (CASE WHEN ( date_selesai = CURDATE() AND date_reschedule >=  CURDATE() AND `status` = 2 ) THEN "d_selesai" 
                    WHEN ( (date_selesai - CURDATE()) <= 2 AND (date_selesai - CURDATE()) >= 1 ) THEN "d_warning"
                    WHEN (date_selesai <= CURDATE()) THEN "d_warning2"
                    WHEN (date_selesai < CURDATE()) THEN "d_terlambat" 
                    ELSE "d_progress" END)
                     as status_data'))
                ->where('list_case.is_archive', 1)
                ->paginate(15);
                
        return view('case.arsip', ['cases' => $cases, 'q'=>array()]);
    }

    public function searching(Request $request) {
        $q = htmlspecialchars($request->q, ENT_QUOTES);

        $subjects_mod = Subjects::orderBy('id', 'desc')
                    ->where('title','LIKE','%'.$q.'%')
                    ->get();

        $lists_id_ar = array();
        foreach ($subjects_mod as $key => $value) {
            $lists_id_ar[] = $value->id;
        }
        $results_tsubj = implode(',', $lists_id_ar);

        $cases2 = Mcase::leftJoin('proyek', 'list_case.proyek_id', '=', 'proyek.id')
                // ->leftJoin('division', 'list_case.divisi_id', '=', 'division.id')
                ->leftJoin('users', 'list_case.divisi_id', '=', 'users.id')
                ->leftJoin('subjects', 'list_case.item_pk_id', '=', 'subjects.id')
                ->orderBy('list_case.id', 'desc')
                ->select( DB::raw('list_case.id, list_case.nama, list_case.no_case, proyek.nama as nama_proyek, users.email as nama_divisi, subjects.title as nama_item, list_case.status, list_case.item_pk_id,
                    (CASE WHEN ( date_selesai = CURDATE() AND date_reschedule >=  CURDATE() AND `status` = 2 ) THEN "d_selesai" 
                    WHEN (date_selesai > CURDATE()) THEN "d_progress" 
                    WHEN (date_selesai < CURDATE()) THEN "d_terlambat" 
                    ELSE "d_baru" END)
                     as status_data'))
                ->whereIn('list_case.item_pk_id', $results_tsubj)
                // ->where('list_case.is_archive', 0)
                ->paginate(15);

        foreach ($cases2 as $key => $value) {
            var_dump( $value->getAttributes() );
        }
        die(0);

        return view('case.index', ['cases' => $cases2, 'q'=>$q]);
    }
 
    public function create() {
        $proyek = Proyek::pluck('nama', 'id')->all();
        // $divisi = Divisi::pluck('name_departement', 'id')->all();
        $member = Member::where('role_member', 1)->pluck('name', 'id')->all();
        $subjects = Subjects::pluck('title', 'id')->all();
        $kawasan = Kawasan::pluck('title', 'id')->all();
        $member = Member::where('role_member', 1)->pluck('email', 'id')->all();
        $blok = Blok::pluck('title', 'title')->all();
        $admin = Admin::where('role_type', '!=', 'manajer_teknik')->pluck('username', 'id_admin')->all();

        $model_total = Mcase::get()->count();
        if ($model_total <= 0) {
            $counters_data = 1;
        } else {
            $last_data = Mcase::orderBy('no_case', 'DESC')->first();
            $counters_data = $last_data->no_case + 1;
        }
        
        $case_no = sprintf("%02d", $counters_data);
        
        // dd(Auth::user()->id_admin);
        // dd($case_no);

        return view('case.create', [ 'proyek'=> $proyek, 'member'=> $member, 'subjects'=> $subjects, 'kawasan'=> $kawasan, 'blok'=> $blok, 'admin'=> $admin, 'no_case'=> $case_no]);
    }
 
    public function edit($id) {
        $cases = Mcase::findOrFail($id);
        $proyek = Proyek::pluck('nama', 'id')->all();
        // $divisi = Divisi::pluck('name_departement', 'id')->all();
        $members = Member::where('role_member', 1)->pluck('name', 'id')->all();
        $subjects = Subjects::pluck('title', 'id')->all();
        $kawasan = Kawasan::pluck('title', 'id')->all();
        $tipes = Tipes::pluck('nama', 'id')->all();
        $blok = Blok::pluck('title', 'title')->all();
        $admin = Admin::where('role_type', '!=', 'manajer_teknik')->pluck('username', 'id_admin')->all();

        return view('case.edit', ['cases' => $cases, 'proyek'=> $proyek, 'member'=>$members, 'subjects'=> $subjects, 'kawasan'=> $kawasan, 'blok'=> $blok, 'admin'=> $admin, 'tipes'=> $tipes]);
    }
 
    public function store(Request $request) {
        $this->validate($request, [
            'foto_sebelum' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'foto_sesudah' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        // echo $icons->getClientOriginalName();
        if ($request->file('foto_sebelum')) {
            $random_sebelum = time() . '_'. rand(100, 3000);
            $foto_sebelum = $request->file('foto_sebelum');
            $input['imagename'] = $random_sebelum .'_'. $foto_sebelum->getClientOriginalName();
            $destinationPath_big = public_path('upload/case/original');
            $foto_sebelum->move($destinationPath_big, $input['imagename']);
            $destinationPath = public_path('upload/case');

            // resize
            $full_file = $destinationPath_big.'/'.$input['imagename'];
            $thumb_name = 'thumb_'. $random_sebelum .'_'.$foto_sebelum->getClientOriginalName();
            $full_thumb = $destinationPath.'/'. $thumb_name;
            $this->Thumbnail($full_file, $full_thumb, 1024);
            $input['imagename'] = $thumb_name;
        }else{
            $input['imagename'] = $request->input('save_image1');
        }

        $input2['imagename'] = '';
        if ($request->file('foto_sesudah')) {
            // $foto_sesudah = $request->file('foto_sesudah');
            // $input2['imagename'] = time() . '_'. rand(100, 3000) .'.'.$foto_sesudah->getClientOriginalExtension();
            // $filename    = rand(100, 3000).'_'.$foto_sesudah->getClientOriginalName();
            // $destinationPath = public_path('upload/case');
            // $foto_sesudah->move($destinationPath, $input2['imagename']);

            $random_sesudah = time() . '_'. rand(100, 3000);
            $foto_sesudah = $request->file('foto_sesudah');
            $input2['imagename'] = $random_sesudah .'_'. $foto_sesudah->getClientOriginalName();
            $destinationPath_big = public_path('upload/case/original');
            $foto_sesudah->move($destinationPath_big, $input2['imagename']);
            $destinationPath = public_path('upload/case');

            // resize
            $full_file = $destinationPath_big.'/'.$input2['imagename'];
            $thumb_name = 'thumb_'. $random_sesudah .'_'.$foto_sesudah->getClientOriginalName();
            $full_thumb = $destinationPath.'/'. $thumb_name;
            $this->Thumbnail($full_file, $full_thumb, 1024);
            $input2['imagename'] = $thumb_name;
        }else{
            $input2['imagename'] = $request->input('save_image2');
        }

        $date_input = date('Y-m-d H:i:s');

        $mod_blok_kavling = $request->blok_v.'/'.$request->kavling_v;

        $data_insert = array_merge($request->all(), ['foto_sebelum' => $input['imagename'], 'foto_sesudah' => $input2['imagename'], 'date_input'=>$date_input, 'blok_kavling'=>$mod_blok_kavling]);

        // dd($data_insert); die(0);

        Mcase::create( $data_insert );

        \Session::flash('notifikasi', 'Data berhasil ditambah.');
        return redirect('backend/admin/cases');
    }

    public function Thumbnail($url, $destination, $width = 300, $height = true) {
        $image = ImageCreateFromString(file_get_contents($url));
        $height = $height === true ? (ImageSY($image) * $width / ImageSX($image)) : $height;

        $output = ImageCreateTrueColor($width, $height);
        ImageCopyResampled($output, $image, 0, 0, 0, 0, $width, $height, ImageSX($image), ImageSY($image));

        ImageJPEG($output, $destination, 90); 

        return $output;
    }
 
    public function update(Request $request, $id) {
        $cases = Mcase::findOrFail($id);

        $save_image = $cases->foto_sebelum;
        $save_image2 = $cases->foto_sesudah;

        if ($request->file('foto_sebelum') or $request->file('foto_sesudah')) {
        
            $this->validate($request, [
                'foto_sebelum' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'foto_sesudah' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $input['imagename'] = '';
            if ($request->file('foto_sebelum')) {
                $random_sebelum = time() . '_'. rand(100, 3000);
                $foto_sebelum = $request->file('foto_sebelum');
                $input['imagename'] = $random_sebelum .'_'. $foto_sebelum->getClientOriginalName();
                $destinationPath_big = public_path('upload/case/original');
                $foto_sebelum->move($destinationPath_big, $input['imagename']);
                $destinationPath = public_path('upload/case');

                // resize
                $full_file = $destinationPath_big.'/'.$input['imagename'];
                $thumb_name = 'thumb_'. $random_sebelum .'_'.$foto_sebelum->getClientOriginalName();
                $full_thumb = $destinationPath.'/'. $thumb_name;
                $this->Thumbnail($full_file, $full_thumb, 1024);
                $input['imagename'] = $thumb_name;
            }else{
                $input['imagename'] = $save_image;
            }

            $input2['imagename'] = '';
            if ($request->file('foto_sesudah')) {
                $random_sesudah = time() . '_'. rand(100, 3000);
                $foto_sesudah = $request->file('foto_sesudah');
                $input2['imagename'] = $random_sesudah .'_'. $foto_sesudah->getClientOriginalName();
                $destinationPath_big = public_path('upload/case/original');
                $foto_sesudah->move($destinationPath_big, $input2['imagename']);
                $destinationPath = public_path('upload/case');

                // resize
                $full_file = $destinationPath_big.'/'.$input2['imagename'];
                $thumb_name = 'thumb_'. $random_sesudah .'_'.$foto_sesudah->getClientOriginalName();
                $full_thumb = $destinationPath.'/'. $thumb_name;
                $this->Thumbnail($full_file, $full_thumb, 1024);
                $input2['imagename'] = $thumb_name;
            }else{
                $input2['imagename'] = $save_image2;
            }

            $mod_blok_kavling = $request->blok_v.'/'.$request->kavling_v;

            $data_update = array_merge($request->all(), ['foto_sebelum' => $input['imagename'], 'foto_sesudah' => $input2['imagename'], 'blok_kavling'=>$mod_blok_kavling] );
        } else {
            
            $mod_blok_kavling = $request->blok_v.'/'.$request->kavling_v;

            $data_update = array_merge($request->all(), ['foto_sebelum' => $save_image, 'foto_sesudah' => $save_image2, 'blok_kavling'=>$mod_blok_kavling]);
        }
        
        // dd($data_update);

        $cases->update($data_update);

        \Session::flash('notifikasi', 'Data berhasil diubah.');
        return redirect('backend/admin/cases');
    }
 
    public function destroy($id) {
        Mcase::destroy($id);

        \Session::flash('notifikasi', 'Data berhasil dihapus.');
        return redirect('backend/admin/cases');
    }

    public function show($id) {
        $cases = Mcase::findOrFail($id);
        $proyek = Proyek::pluck('nama', 'id')->all();
        $subjects = Subjects::pluck('title', 'id')->all();
        // get kawasan from proyek id
        $kawasan = Kawasan::where('proyek_id', $cases->proyek_id)->get();
        // get tipes from proyek id and kawasan id
        $blok = Blok::pluck('title', 'title')->all();
        $admin = Admin::where('role_type', '!=', 'manajer_teknik')->pluck('username', 'id_admin')->all();

        // $divisi = Divisi::pluck('name_departement', 'id')->all();
        $members = Member::where('role_member', 1)->pluck('name', 'id')->all();

        $t_where = [
                    ['proyek_id', '=', $cases->proyek_id],
                    ['kawasan_id', '=', $cases->kawasan],
                ];
        $tipes = Tipes::where($t_where)->get();

        return view('case.edit', ['cases' => $cases, 'proyek'=> $proyek, 'member'=>$members, 'subjects'=> $subjects, 'kawasan'=> $kawasan, 'blok'=> $blok, 'admin'=> $admin, 'tipes'=> $tipes]);
    }

    public function lists_data()
    {        
        $n_divisi = ($_GET['divisi']);

        $data_member = DB::table('users')
            ->select('users.*')
            ->where('users.id', $n_divisi)
            ->get()->first();
        $divisi = $data_member->email;
        
        $n_proyek = 1;
        
        $mproyek = DB::table('proyek')
            ->select('proyek.*')
            ->where('proyek.id', $n_proyek)
            ->get()->first();
        $mproyek = $mproyek->nama;

        // Get user staff
        $model = Mcase::leftJoin('proyek', 'list_case.proyek_id', '=', 'proyek.id')
                ->leftJoin('users', 'list_case.divisi_id', '=', 'users.id')
                ->leftJoin('subjects', 'list_case.item_pk_id', '=', 'subjects.id')
                ->select( DB::raw('list_case.*, subjects.title as subject,
                    (CASE WHEN ( `status` = 2 ) THEN "d_selesai" 
                    WHEN ( (date_selesai - CURDATE()) <= 2 AND (date_selesai - CURDATE()) >= 1 ) THEN "d_warning"
                    WHEN (date_selesai <= CURDATE() AND `status` = 1) THEN "d_warning2"
                    WHEN (date_selesai < CURDATE()) THEN "d_terlambat" 
                    ELSE "d_progress" END)
                     as status_data') )
                ->where('list_case.is_archive', 0)
                ->where('list_case.proyek_id', $n_proyek)
                ->where('list_case.divisi_id', $n_divisi)
                // ->orderBy('list_case.id', 'desc')
                ->orderBy('list_case.blok_kavling', 'asc')
                ->paginate(15);
        
        if (isset($_GET['divisi']) AND $_GET['divisi'] != '') {
            $model->appends(['divisi' => $_GET['divisi'] ]);
        }
        // dd($model);

        return view('case.indexes_2', ['model' => $model, 'divisi'=>$divisi, 'mproyek'=>$mproyek]);
    }

    public function lists_data2()
    {        
        $n_admin = ($_GET['spv_id']);

        $data_member = DB::table('admin')
            ->select('admin.*')
            ->where('admin.id_admin', $n_admin)
            ->get()->first();
        $divisi = $data_member->email;
        
        $n_proyek = 1;
        
        $mproyek = DB::table('proyek')
            ->select('proyek.*')
            ->where('proyek.id', $n_proyek)
            ->get()->first();
        $mproyek = $mproyek->nama;

        // Get user staff
        $model = Mcase::leftJoin('proyek', 'list_case.proyek_id', '=', 'proyek.id')
                ->leftJoin('admin', 'list_case.users_id', '=', 'admin.id_admin')
                ->leftJoin('subjects', 'list_case.item_pk_id', '=', 'subjects.id')
                ->select( DB::raw('list_case.*, subjects.title as subject,
                    (CASE WHEN ( `status` = 2 ) THEN "d_selesai" 
                    WHEN ( (date_selesai - CURDATE()) <= 2 AND (date_selesai - CURDATE()) >= 1 ) THEN "d_warning"
                    WHEN (date_selesai <= CURDATE() AND `status` = 1) THEN "d_warning2"
                    WHEN (date_selesai < CURDATE()) THEN "d_terlambat" 
                    ELSE "d_progress" END)
                     as status_data') )
                ->where('list_case.is_archive', 0)
                ->where('list_case.proyek_id', $n_proyek)
                ->where('list_case.users_id', $n_admin)
                // ->orderBy('list_case.id', 'desc')
                ->orderBy('list_case.blok_kavling', 'asc')
                ->paginate(15);
        
        if (isset($_GET['divisi']) AND $_GET['divisi'] != '') {
            $model->appends(['divisi' => $_GET['divisi'] ]);
        }
        // dd($model);

        return view('case.indexes_3', ['model' => $model, 'divisi'=>$divisi, 'mproyek'=>$mproyek]);
    }

    public function case_edit_manager()
    {
        $id = isset($_GET['id'])? intval($_GET['id']): '';
        
        $model = Mcase::findOrFail($id);

        // $divisi = Divisi::pluck('name_departement', 'id')->all();
        $divisi = Member::pluck('name', 'id')->all();
        $subjects = Subjects::pluck('title', 'id')->all();
        $proyek = Proyek::pluck('nama', 'id')->all();
        $kawasan = Kawasan::pluck('title', 'id')->all();
        $tipes = Tipes::pluck('nama', 'id')->all();
        $blok = Blok::pluck('title', 'title')->all();
        // $admin = Admin::pluck('username', 'id_admin')->all();
        $admin = Admin::where('role_type', '!=', 'manajer_teknik')->pluck('username', 'id_admin')->all();

        if ( $model->date_selesai ) {
            $model->date_selesai = date('d-M-y', strtotime($model->date_selesai));
        }

        $case_kawasanTipe = DB::table('users')->get();

        return view('case.edit_manager', ['cases' => $model, 'proyek'=> $proyek, 'divisi'=>$divisi, 'subjects'=> $subjects, 'kawasan'=> $kawasan, 'tipes'=> $tipes, 'blok'=>$blok, 'admin'=> $admin]);
    }

}
