<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

use Auth;
use App\Kategori;
use App\Posts;
use App\ListAktivitas;
use App\Setuplog;
use App\Grouping;
use App\Category;

use App\Member;
use App\Divisi;
use App\Proyek;
use App\Mcase;
use App\Subjects;
use App\Kawasan;
use App\Tipes;
use App\Admin;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mproyek = null;
        // $all_divisi = Divisi::all();
        $all_divisi = Member::all();
        $nget_divisi = isset($_GET['division'])? intval($_GET['division']) : null;

        $all_proyek = Proyek::all();
        $nget_proyek = isset($_GET['proyeks'])? intval($_GET['proyeks']) : null;

        $ids_member = Auth::user()->email;
        $data_member = DB::table('users')
            ->select('users.*')
            ->where('users.email', $ids_member)
            ->get()->first();

        // GM Proyek
        if ($data_member->role_member == 4) {
            return $this->homeGm();

        }elseif ($data_member->role_member == 3) {

            $nfilter_divisi = array();
            if ( isset($nget_divisi) ) {
                $nfilter_divisi = ['list_case.divisi_id', '=', $nget_divisi];
            }
            
            $nfilter_proyek = array();
            if ( isset($nget_divisi) ) {
                $nfilter_proyek = ['list_case.proyek_id', '=', $nget_proyek];
            }

            $wheres_datas = array_filter([
                            ['list_case.is_archive', '=', 0],
                            $nfilter_proyek,
                            $nfilter_divisi
                           ]);
            
            // Get user staff
            $model = Mcase::leftJoin('proyek', 'list_case.proyek_id', '=', 'proyek.id')
                    ->leftJoin('division', 'list_case.divisi_id', '=', 'division.id')
                    ->leftJoin('subjects', 'list_case.item_pk_id', '=', 'subjects.id')
                    ->orderBy('list_case.id', 'desc')
                   ->select( DB::raw('list_case.id, list_case.nama, list_case.no_case, proyek.nama as nama_proyek, division.name_departement as nama_divisi, list_case.penyebab, list_case.cara_perbaikan, list_case.foto_sebelum, list_case.foto_sesudah, list_case.status, subjects.title as subject, subjects.title as content,
                    (CASE WHEN ( `status` = 2 ) THEN "d_selesai" 
                    WHEN ( (date_selesai - CURDATE()) <= 2 AND (date_selesai - CURDATE()) >= 1 ) THEN "d_warning"
                    WHEN (date_selesai <= CURDATE() AND `status` = 1) THEN "d_warning2"
                    WHEN (date_selesai < CURDATE()) THEN "d_terlambat" 
                    ELSE "d_progress" END)
                     as status_data') )
                    ->where($wheres_datas)
                    ->paginate(15);

            $model2 = Mcase::leftJoin('proyek', 'list_case.proyek_id', '=', 'proyek.id')
                    ->leftJoin('division', 'list_case.divisi_id', '=', 'division.id')
                    ->leftJoin('subjects', 'list_case.item_pk_id', '=', 'subjects.id')
                    ->orderBy('list_case.id', 'desc')
                    ->select( DB::raw('list_case.id, list_case.nama, list_case.no_case, proyek.nama as nama_proyek, division.name_departement as nama_divisi, list_case.penyebab, list_case.cara_perbaikan, list_case.foto_sebelum, list_case.foto_sesudah, list_case.status, subjects.title as subject, list_case.content as content') )
                    ->where($wheres_datas)
                    ->get();
            
            $ls_res_data = [];
            $open = 0;
            $close = 0;
            $nls = [];
            if ($model2->count() > 0) {
                foreach ($model2 as $key => $value) {
                    $nls[] = $value->status;
                }
            }
            $ls_res_data = array_count_values($nls);

        } else {
            // $divisi = DB::table('division')
            //     ->select('division.*')
            //     ->where('division.id', $data_member->division)
            //     ->get()->first();
            // $divisi = $divisi->name_departement;
            $divisi = $data_member->name;

            $data_proyek = DB::table('proyek')
                ->select('proyek.*')
                ->where('proyek.id', $data_member->proyek_id)
                ->get()->first();
            $mproyek = $data_proyek->nama;

            // Get user staff
            $model = Mcase::leftJoin('proyek', 'list_case.proyek_id', '=', 'proyek.id')
                    ->leftJoin('division', 'list_case.divisi_id', '=', 'division.id')
                    ->leftJoin('subjects', 'list_case.item_pk_id', '=', 'subjects.id')
                    ->orderBy('list_case.blok_kavling', 'asc')
                    ->select( DB::raw('list_case.id, list_case.nama, list_case.no_case, proyek.nama as nama_proyek, division.name_departement as nama_divisi, list_case.penyebab, list_case.cara_perbaikan, list_case.foto_sebelum, list_case.foto_sesudah, list_case.status, subjects.title as subject, list_case.content as content,
                        (CASE WHEN ( `status` = 2 ) THEN "d_selesai" 
                        WHEN ( (date_selesai - CURDATE()) <= 2 AND (date_selesai - CURDATE()) >= 1 ) THEN "d_warning"
                        WHEN (date_selesai <= CURDATE() AND `status` = 1) THEN "d_warning2"
                        WHEN (date_selesai < CURDATE()) THEN "d_terlambat" 
                        ELSE "d_progress" END)
                         as status_data, list_case.blok_kavling') )
                    ->where([
                        // ['list_case.divisi_id', '=', $data_member->division],
                        ['list_case.divisi_id', '=', $data_member->id],
                        ['list_case.proyek_id', '=', $data_member->proyek_id],
                        ['list_case.is_archive', '=', 0],
                    ])
                    ->paginate(15);
        }
        

        $nfilter_audit = array();
        if ($data_member->role_member == 4) {
            $nfilter_audit = ['list_case.proyek_id', '=', $data_member->proyek_id];
        }

        $wheres_data = array_filter([
                        ['list_case.is_archive', '=', 0],
                        $nfilter_audit
                       ]);

        // dd($ls_res_data);
        if (isset($_GET['proyek']) AND $_GET['proyek'] != '') {
            $model->appends(['proyek' => $_GET['proyek'], 'divisi' => isset($_GET['divisi'])? $_GET['divisi']: ''  ]);
        }

        if ($data_member->role_member == 1 || $data_member->role_member == 2) {
            return view('home', ['model' => $model, 'divisi'=>$divisi, 'nget_divisi'=> $nget_divisi]);
        } else {
            return view('home_manager', ['n_model'=>$model2, 'model' => $ls_res_data, 'mproyek'=>$mproyek, 'm_divisi'=>$all_divisi, 'nget_divisi'=> $nget_divisi, 'm_proyek'=>$all_proyek, 'nget_proyek'=>$nget_proyek ]);
        }
    }

    public function lists_index()
    {
        $ids_member = Auth::user()->email; 
        $data_member = DB::table('users')
            ->select('users.*')
            ->where('users.email', $ids_member)
            ->get()->first();

        $n_divisi = null;
        if ($data_member->role_member == 4 || $data_member->role_member == 3 || $data_member->role_member == 1) {
            $n_divisi = $_GET['divisi'];
        }else{
            // $n_divisi = $data_member->division;
            $n_divisi = $data_member->id;
        }
        // var_dump($n_divisi); exit;

        $divisi = $data_member->email;
        // dd($divisi);

        $n_proyek = null;
        if ($data_member->role_member == 4) {
            $n_proyek = $data_member->proyek_id;
        }elseif ($data_member->role_member == 3) {
            $n_proyek = $_GET['proyek'];
        }else{
            $n_proyek = $data_member->proyek_id;
        }
        
        $mproyek = DB::table('proyek')
            ->select('proyek.*')
            ->where('proyek.id', $n_proyek)
            ->get()->first();
        $mproyek = $mproyek->nama;
            
        $model = Mcase::leftJoin('proyek', 'list_case.proyek_id', '=', 'proyek.id')
                ->leftJoin('users', 'list_case.divisi_id', '=', 'users.id')
                ->leftJoin('subjects', 'list_case.item_pk_id', '=', 'subjects.id')
                ->orderBy('list_case.blok_kavling', 'asc')
                ->select( DB::raw('list_case.id, list_case.nama, list_case.no_case, proyek.nama as nama_proyek, users.email as nama_divisi, list_case.penyebab, list_case.cara_perbaikan, list_case.foto_sebelum, list_case.foto_sesudah, list_case.status, list_case.content as content, subjects.title as subject,
                    (CASE WHEN ( `status` = 2 ) THEN "d_selesai" 
                    WHEN ( (date_selesai - CURDATE()) <= 2 AND (date_selesai - CURDATE()) >= 1 ) THEN "d_warning"
                    WHEN (date_selesai <= CURDATE() AND `status` = 1) THEN "d_warning2"
                    WHEN (date_selesai < CURDATE()) THEN "d_terlambat" 
                    ELSE "d_progress" END)
                     as status_data, list_case.blok_kavling') )
                ->where('list_case.is_archive', 0)
                ->where('list_case.proyek_id', $n_proyek)
                ->where('list_case.divisi_id', $n_divisi)
                ->paginate(15);
        
        if (isset($_GET['proyek']) AND $_GET['proyek'] != '') {
            $model->appends(['proyek' => $_GET['proyek'], 'divisi' => isset($_GET['divisi'])? $_GET['divisi']: ''  ]);
        }
        if (isset($_GET['divisi']) AND $_GET['divisi'] != '') {
            $model->appends(['divisi' => $_GET['divisi'] ]);
        }

        return view('home_index2', ['model' => $model, 'divisi'=>$divisi, 'mproyek'=>$mproyek]);
    }

    public function case_edit()
    {
        $id = isset($_GET['id'])? intval($_GET['id']): '';
        
        $model = Mcase::findOrFail($id);

        $admin = Admin::where('role_type', '!=', 'manajer_teknik')->pluck('username', 'id_admin')->all();
        $divisi = Member::pluck('name', 'id')->all();
        $subjects = Subjects::pluck('title', 'id')->all();
        $proyek = Proyek::pluck('nama', 'id')->all();
        $kawasan = Kawasan::pluck('title', 'id')->all();
        $tipes = Tipes::pluck('nama', 'id')->all();

        if ( $model->date_selesai ) {
            $model->date_selesai = date('d-M-y', strtotime($model->date_selesai));
        }

        $case_kawasanTipe = DB::table('users')->get();

        return view('case_f.edit', ['cases' => $model, 'proyek'=> $proyek, 'divisi'=>$divisi, 'subjects'=> $subjects, 'kawasan'=> $kawasan, 'tipes'=> $tipes, 'admin'=> $admin]);
    }

    public function homeGm()
    {
        $mproyek = null;
        // $all_divisi = Divisi::all();
        $all_divisi = Member::where('role_member', 1)->get();
        $nget_divisi = isset($_GET['division'])? intval($_GET['division']) : null;

        $all_proyek = Proyek::all();
        $nget_proyek = isset($_GET['proyeks'])? intval($_GET['proyeks']) : null;

        $ids_member = Auth::user()->email;
        $data_member = DB::table('users')
            ->select('users.*')
            ->where('users.email', $ids_member)
            ->get()->first();

         $data_proyek = DB::table('proyek')
            ->select('proyek.*')
            ->where('proyek.id', $data_member->proyek_id)
            ->get()->first();
        $mproyek = $data_proyek->nama;

        $results = [];
        foreach ($all_divisi as $key => $value) {

        // Get user staff
        $sub_model = Mcase::select( DB::raw('list_case.id, list_case.nama, list_case.no_case, list_case.penyebab, list_case.cara_perbaikan, list_case.foto_sebelum, list_case.foto_sesudah, list_case.status, list_case.content as content') )
            ->where([
                    ['list_case.is_archive', '=', 0],
                    ['list_case.proyek_id', '=', $data_member->proyek_id],
                    ['list_case.divisi_id', '=', $value->id]
                    ])
            ->get();

            $open = 0;
            $close = 0;
            $nls = [];
            foreach ($sub_model as $keys => $values) {
                $nls[] = $values->status;
            }
            $ls_res_data = array_count_values($nls);

            $results[] = [
                        'divisi_id' => $value->id,
                        'divisi_name' => $value->name,
                        'status' => $ls_res_data,
                        'total_data' => count($sub_model),
                        'open'=>isset($ls_res_data[1])? $ls_res_data[1] : 0,
                        'close'=>isset($ls_res_data[2])? $ls_res_data[2] : 0,
                        ];
        }
        // dd($results);
        return view('home_manager2', [ 'n_model'=>$results, 'mproyek'=>$mproyek, ]);

    }

    public function homeGm_other()
    {
        $mproyek = null;
        // $all_divisi = Divisi::all();
        $all_divisi = Member::where('role_member', 1)->get();
        $nget_divisi = isset($_GET['division'])? intval($_GET['division']) : null;

        $all_proyek = Proyek::all();
        $nget_proyek = isset($_GET['proyeks'])? intval($_GET['proyeks']) : null;

        $ids_member = Auth::user()->email;
        $data_member = DB::table('users')
            ->select('users.*')
            ->where('users.email', $ids_member)
            ->get()->first();

         $data_proyek = DB::table('proyek')
            ->select('proyek.*')
            ->where('proyek.id', $data_member->proyek_id)
            ->get()->first();
        $mproyek = $data_proyek->nama;

        $results = [];
        foreach ($all_divisi as $key => $value) {

        // Get user staff
        $sub_model = Mcase::select( DB::raw('list_case.id, list_case.nama, list_case.no_case, list_case.penyebab, list_case.cara_perbaikan, list_case.foto_sebelum, list_case.foto_sesudah, list_case.status, list_case.content as content') )
            ->where([
                    ['list_case.is_archive', '=', 0],
                    ['list_case.proyek_id', '=', $data_member->proyek_id],
                    ['list_case.divisi_id', '=', $value->id]
                    ])
            ->get();

            $open = 0;
            $close = 0;
            $nls = [];
            foreach ($sub_model as $keys => $values) {
                $nls[] = $values->status;
            }
            $ls_res_data = array_count_values($nls);

            $results[] = [
                        'divisi_id' => $value->id,
                        'divisi_name' => $value->name,
                        'status' => $ls_res_data,
                        'total_data' => count($sub_model),
                        'open'=>isset($ls_res_data[1])? $ls_res_data[1] : 0,
                        'close'=>isset($ls_res_data[2])? $ls_res_data[2] : 0,
                        ];
        }
        // dd($results);
        return view('home_manager2_other', [ 'n_model'=>$results, 'mproyek'=>$mproyek, ]);

    }

    public function testing()
    {
        $ncase_id = DB::table('list_case')->where('id', 2)->first();

        // return redirect()->route('list_indexs', ['divisi'=>$ncase_id->divisi_id, 'proyek'=>$ncase_id->proyek_id]);
        $nurl_red = url('/').'/case_list/indexs?divisi='.$ncase_id->divisi_id.'&proyek='.$ncase_id->proyek_id;
        redirect()->to($nurl_red)->send();
    }

    public function case_edit_p(Request $request)
    {
        $this->validate($request, [
            'foto_sebelum' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'foto_sesudah' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $input2['imagename'] = '';
        if ($request->file('foto_sesudah')) {
            $foto_sesudah = $request->file('foto_sesudah');
            $input2['imagename'] = time() . '_'. rand(100, 3000) .'.'.$foto_sesudah->getClientOriginalExtension();
            $destinationPath = public_path('upload/case');
            $foto_sesudah->move($destinationPath, $input2['imagename']);
        }

        if (Auth::user()->role_member == 3) {

            DB::table('list_case')->where('id',$request->id)->update([
                'cara_perbaikan' => $request->cara_perbaikan,
                // 'notes' => $request->notes,
                'status' => $request->status,
            ]);

            $ncase_id = DB::table('list_case')->where('id', $request->id)->first();
            $nurl_red = url('/').'/case_list/indexs?divisi='.$ncase_id->divisi_id.'&proyek='.$ncase_id->proyek_id;
            redirect()->to($nurl_red)->send();
        }else{

            if ($request->cara_perbaikan != '') {
                DB::table('list_case')->where('id', $request->id)->update([
                    // 'penyebab' => $request->penyebab,
                    'cara_perbaikan' => $request->cara_perbaikan,
                    // 'date_selesai' => date("Y-m-d", strtotime($request->date_selesai) ),
                    // 'date_reschedule' => $request->date_reschedule,
                    // 'notes' => $request->notes,
                    // 'status' => $request->status,
                    // 'foto_sesudah' => $input2['imagename'],
                ]);
                
            }else{
                if ($input2['imagename']) {
                    DB::table('list_case')->where('id', $request->id)->update([
                        'penyebab' => $request->penyebab,
                        // 'date_selesai' => date("Y-m-d", strtotime($request->date_selesai) ),
                        // 'date_reschedule' => $request->date_reschedule,
                        // 'notes' => $request->notes,
                        // 'status' => $request->status,
                        'foto_sesudah' => $input2['imagename'],
                    ]);
                }
            }

            return redirect('/home');
        }

    }

    public function landing()
    {
        $id = isset($_GET['id'])? intval($_GET['id']): '';
        
        $parent_cat = Category::findOrFail($id);
        
        $kategori = Category::where('parent_id', '=', $id)->get();

        return view('listing.landing', ['kategori' => $kategori, 'parent_cat'=> $parent_cat]);
    }

    public function listing()
    {
        $parent_cat = isset($_GET['parent'])? intval($_GET['parent']): '';
        $parents = Category::findOrFail($parent_cat);

        $id = isset($_GET['id'])? intval($_GET['id']): '';
        $kategori = Category::findOrFail($id);

        // type artikel / video
        $_GET['type'] = isset($_GET['type'])? $_GET['type']: '';
        if ($_GET['type'] == 'video') {
            $n_page = 'listing.video_list';
            $matchThese = ['is_video'=> 1];
        }else{
            $n_page = 'listing.list';
            $matchThese = ['is_video'=> 0];
        }
        // get cat id sub categories
        $sn_arr1 = DB::table('categories')->select('id')->where('parent_id',  $id)->get();
        $res_ar1 = array();
        if (count($sn_arr1) > 0) {
            foreach ($sn_arr1 as $key => $value) {
                $res_ar1[] = $value->id;
            }
        }
        $res_ar1[] = $id;

        $data = DB::table('posts')->join('categories', 'categories.id', '=', 'posts.category_id')->where($matchThese)->whereIn('posts.category_id', $res_ar1)->orderBy('posts.id', 'asc')->select( DB::raw('posts.id, posts.title, posts.views, categories.title as names_category') )->paginate(15);

        $tgroup = Grouping::pluck('names', 'id')->all();
        $ac_tgroup = isset($_GET['group'])? $_GET['group'] : false;

        // if (isset($_GET['group']) AND $_GET['id'] != '') {
        //     $data = Posts::leftJoin('grouping', 'posts.tgroup', '=', 'grouping.id')
        //      ->orderBy('grouping.names', 'asc')
        //      ->select( DB::raw('posts.id, posts.title, posts.video, grouping.names as names_group') )
        //      ->where('tgroup', '=', $_GET['group'])
        //      ->where($matchThese)
        //      ->paginate(50);
        // }
        // print_r($data);
        // exit;

        return view($n_page, ['kategori'=>$kategori, 'data'=>$data, 'tgroup'=>$tgroup, 'ac_tgroup'=> $ac_tgroup, 'parent_cat'=>$parents]);
    }

    public function detail()
    {
        
        $parent_cat = isset($_GET['parent'])? intval($_GET['parent']): '';
        $parents = Category::findOrFail($parent_cat);

        $id_kat = isset($_GET['kategori'])? $_GET['kategori']: '';
        $kategori = Category::findOrFail($id_kat);

        $post_id = isset($_GET['id'])? intval($_GET['id']): '';
        $post = Posts::findOrFail($post_id);

        if ($post->video) {
            parse_str( parse_url($post->video, PHP_URL_QUERY ), $my_array_of_vars );
            $post->video =  $my_array_of_vars['v'];
        }

        $setupLog = Setuplog::findOrFail(1);
        if ($setupLog->status == 1) {
            $log_activity = new ListAktivitas;
            $log_activity->user_id = Auth::user()->id;
            $log_activity->post_id = $post_id;
            $log_activity->save();

            $jumlah_view = $post->views + 1;
            $data_p = array( 'views'=> $jumlah_view );
            Posts::where('id' , $post_id)->update($data_p);
        }
        // type artikel / video
        return view('listing.detail', ['data'=>$post, 'kategori'=> $kategori, 'parent_cat'=>$parents]);
    }

    public function search(Request $request) {
        $query = $request->post('search');

        $data=Posts::where('title','LIKE','%'.$query.'%')->orwhere('content','LIKE','%'.$query.'%')->get();
        $data_kat=Posts::where('title','LIKE','%'.$query.'%')->orwhere('content','LIKE','%'.$query.'%')->get()->first();

        if ($data_kat) {
            $kategori = Category::findOrFail($data_kat->category_id);
        }else{
            $kategori = false;
        }

        return view('front.search', ['kategori'=> $kategori, 'data' => $data, 'searchs'=>$query]);
    }

    public function callkawasan()
    {
        $data_kawasan = Kawasan::all();
        $arr = array();
        exit;
        foreach ($data_kawasan as $key => $value) {
            $proy = new Proyek;
            $proy->nama = $value->title;
            $proy->info = $value->title;
            $proy->save();
        }

        exit;

    }


}
