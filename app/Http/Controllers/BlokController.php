<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Blok;
use Auth;

class BlokController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index() {
        $blok = Blok::orderBy('id', 'asc')->paginate(10);
        return view('blok.index', ['blok' => $blok, 'q'=>array()]);
    }

    public function searching(Request $request) {
        $q = htmlspecialchars($request->q, ENT_QUOTES);

        $blok = Blok::orderBy('id', 'asc')->where('title','LIKE','%'.$q.'%')->paginate(10);
        return view('blok.index', ['blok' => $blok, 'q'=> $q]);
    }
 
    public function create() {
        return view('blok.create');
    }
 
    public function edit($id) {
        $blok = Blok::findOrFail($id);
        return view('blok.edit', ['blok' => $blok]);
    }
 
    public function store(Request $request) {
        $request->validate([
            'title' => 'required|unique:blok|max:100',
        ]);

        Blok::create($request->all());
        \Session::flash('notifikasi', 'Data berhasil ditambah.');
        return redirect('backend/admin/blok');
    }
 
    public function update(Request $request, $id) {
        $blok = Blok::findOrFail($id);
        $blok->update($request->all());
        \Session::flash('notifikasi', 'Data berhasil diubah.');
        return redirect('backend/admin/blok');
    }
 
    public function destroy($id) {
        Blok::destroy($id);
        \Session::flash('notifikasi', 'Data berhasil dihapus.');
        return redirect('backend/admin/blok');
    }

    public function show($id) {

    }

}
