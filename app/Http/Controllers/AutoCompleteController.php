<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Posts;

class AutoCompleteController extends Controller {
    
    public function index(){
        return view('autocomplete.index');
   }

    public function autoComplete(Request $request) {
        $query = $request->get('term','');
        
        $posts=Posts::where('content','LIKE','%'.$query.'%')->orwhere('content','LIKE','%'.$query.'%')->get();
        
        $data=array();
        foreach ($posts as $post_dt) {
                $data[]=array('value'=>$post_dt->title,'id'=>$post_dt->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }
    
}