<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use App\Mcase;
use App\Divisi;
use App\Proyek;
use App\Kawasan;
use App\Tipes;
use Auth;

class CustomController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function nocase()
    {
        $id = intval($_GET['proyek_id']);
        $s_case = Mcase::where('proyek_id', '=', $id)->get();
        if ($s_case->count() <= 0) {
            $wordCount = $s_case->count() + 1;
        } else {
            $last_data = Mcase::orderBy('no_case', 'DESC')->first();
            $wordCount = $last_data->no_case + 1;
        }
        
        // $s_proyek = Proyek::where('id', '=', $id)->first();
        // $data = $s_proyek->nama.'-0'.$wordCount;
        if ($wordCount < 10) {
            $data = '0'.$wordCount;
        } else {
            $data = $wordCount;
        }
        

        return json_encode($data);
    }

    public function index() {
        $n_member = Member::pluck('name', 'id')->all();
    	$n_proyek = Proyek::pluck('nama', 'id')->all();
        return view('listaktivitas.index', ['member' => $n_member, 'proyek' => $n_proyek]);
    }

    public function showkawasan()
    {
        $id = intval($_GET['proyek_id']);
        $lists_kaw = Kawasan::where('proyek_id', '=', $id)->get();

        return json_encode($lists_kaw);
    }

    public function showtipe()
    {
        $proyek_id = intval($_GET['proyek_id']);
        $kawasan_id = intval($_GET['kawasan_id']);
        $db_where = [
                    ['proyek_id', '=', $proyek_id],
                    ['kawasan_id', '=', $kawasan_id],
                    ];
        $lists_tipes = Tipes::where($db_where)->get();

        return json_encode($lists_tipes);
    }
    

}