<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use App\Mcase;
use App\Divisi;
use App\Member;
use App\Subjects;
use App\Proyek;
use App\Kawasan;
use App\Tipes;
use App\Admin;

// use Excel;
use App\Exports\UserReport;
use Maatwebsite\Excel\Facades\Excel;

use Auth;

class ReportController extends Controller
{
    public function __construct()
    {
        Auth::shouldUse('admin');
        $this->middleware('auth.admin');
    }

    public function index() {
        $n_member = Member::pluck('name', 'id')->all();
    	$n_proyek = Proyek::pluck('nama', 'id')->all();
        $n_divisi = Divisi::pluck('name_departement', 'id')->all();

        // var_dump(Auth::user()->username);
        // exit;
        // $admin_divisi = Subjects::here('title', $var_get)
        //                 ->get();

        $model = Mcase::leftJoin('proyek', 'list_case.proyek_id', '=', 'proyek.id')
                ->leftJoin('users', 'list_case.divisi_id', '=', 'users.id')
                ->leftJoin('subjects', 'list_case.item_pk_id', '=', 'subjects.id')
                ->orderBy('list_case.id', 'desc')
                ->select( DB::raw('list_case.id, list_case.nama, list_case.no_case, proyek.nama as nama_proyek, users.email as nama_divisi, subjects.title as nama_item, list_case.status, list_case.content as contents, list_case.penyebab as penyebab, list_case.cara_perbaikan as cara_perbaikan, list_case.foto_sebelum, list_case.foto_sesudah
                    , (CASE WHEN ( date_selesai = CURDATE() AND date_reschedule >=  CURDATE() AND `status` = 2 ) THEN "d_selesai" 
                    WHEN (date_selesai > CURDATE()) THEN "d_progress" 
                    WHEN (date_selesai < CURDATE()) THEN "d_terlambat" 
                    ELSE "d_baru" END)
                     as status_data'))
                ->where([
                    ['list_case.is_archive', '=', 0],
                    ['list_case.status', '=', 2],
                ])->paginate(10);

        return view('report.index', ['model'=>$model, 'member' => $n_member, 'proyek' => $n_proyek, 'divisi'=>$n_divisi]);
    }

    public function excel()
    {
        // work it
        return Excel::download( new UserReport, 'users.xlsx');
    }

}