<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Route::get('/general_manager', 'HomeController@homeGm')->name('home_manager');

Route::get('/case_other', 'HomeController@homeGm_other')->name('case_other');

Route::get('/home', 'HomeController@index')->name('home')->name('search_kaw');

Route::get('/landing', 'HomeController@landing')->name('landing');

Route::get('/list', 'HomeController@listing')->name('list');

Route::get('/detail', 'HomeController@detail')->name('detail');

Route::get('/callkawasan', 'HomeController@callkawasan')->name('callkawasan');

Route::resource('/karyawan', 'KaryawanController');

Route::get('/searchajax',array('as'=>'searchajax','uses'=>'AutoCompleteController@autoComplete'));

Route::get('/search' , 'HomeController@search');

Route::get('/testing' , 'HomeController@testing');

Route::get('/case_edit', 'HomeController@case_edit')->name('case_edit');

Route::post('/case_list/update', 'HomeController@case_edit_p');

Route::get('/case_list/indexs', 'HomeController@lists_index')->name('list_indexs');

Route::get('/reset_pass', 'ResetpassController@resetpass');

Auth::routes();

Route::prefix('backend')->group(function(){

	Route::get('/admin/logaktivitas' , 'ListaktivitasController@index');
	Route::post('/admin/logaktivitas/showdata' , 'ListaktivitasController@showdata');
	Route::post('/admin/logaktivitas/changeProyek', 'ListaktivitasController@changeProyek')->name('admin.changeproyek');
	Route::get('/admin/logaktivitas/showdetail', 'ListaktivitasController@showDetail')->name('admin.showdetail');

	Route::resource('/admin/posting', 'PostingController');

	Route::resource('/admin/kategori', 'KategoriController');
	Route::resource('/admin/divisi', 'DivisiController');

	// New case Ciputra
	Route::resource('/admin/cases', 'CasesController');

	Route::get('/admin/manager/list','CasesController@lists_data')->name('cases.lists_data');
	Route::get('/admin/manager/case_edit', 'CasesController@case_edit_manager')->name('case_edit_manager');

	Route::get('/admin/manager/list2','CasesController@lists_data2')->name('cases.lists_data2');

	// Route::get('/admin/custom/nocase', 'CustomController@nocase')->name('customs.nocases');

	// Route::patch('/admin/cases/{id}',[
	//     'as' => 'admin.cases.edits',
	//     'uses' => 'CasesController@update'
	// ]);

	Route::get('/admin/arsip/list','CasesController@arsips')->name('cases.arsip');
	
	Route::resource('/admin/proyek', 'ProyekController');

	Route::resource('/admin/member', 'MemberController');

	Route::resource('/admin/subjectc', 'SubjectcController');
	Route::post('/admin/subjectc/search' , 'SubjectcController@searching')->name('admin.searchsubject');

	Route::resource('/admin/blok', 'BlokController');
	Route::post('/admin/blok/search' , 'BlokController@searching')->name('admin.bloksubject');

	Route::resource('/admin/kawasan', 'KawasanController');
	Route::get('/admin/kawasan' , 'KawasanController@index')->name('admin.searchkawasan');

	Route::resource('/admin/tipes', 'TipesController');
	Route::get('/admin/tipes' , 'TipesController@index')->name('admin.tipesindexs');

	Route::resource('/admin/setuplog', 'SetuplogController');
	Route::resource('/admin/grouping', 'GroupingController');

	Route::get('/admin/admins' , 'AdminController@index2');
	Route::post('/admin/admins/postInsert' , 'AdminController@postInsert');
	Route::get('/admin/admins/edit/{id}' , 'AdminController@edit');
	Route::post('/admin/admins/update/{id}' , 'AdminController@update');
	Route::get('/admin/admins/delete/{id}' , 'AdminController@delete');

	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
	Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

	Route::get('/','AdminController@index')->name('admin.dashboard');

	Route::post('ajaximage', function(){
		$file = Request::file('file');
		$destinationPath = public_path().'/uploads/';
		$filename = $file->getClientOriginalName();
		$file->move($destinationPath, $filename);
		echo url().'/uploads/'. $filename;
	});

	Route::get('/admin/category/index',['uses'=>'CategoryController@index'])->name('category.index');
	Route::get('/admin/category/edit',['uses'=>'CategoryController@edit'])->name('category.edit');
	Route::get('/admin/category/delete',['uses'=>'CategoryController@delete'])->name('category.delete');
	
	Route::get('/admin/category-tree-view',['uses'=>'CategoryController@manageCategory']);
	Route::get('/admin/category-tree-view2',['uses'=>'CategoryController@treeView']);

	Route::post('/admin/edit-category',['as'=>'edit.category','uses'=>'CategoryController@editData']);

	Route::post('/admin/add-category',['as'=>'add.category','uses'=>'CategoryController@addCategory']);

	Route::get('/admin/custom/nocase', 'CustomController@nocase')->name('customs.nocases');
	Route::get('/admin/custom/showkawasan', 'CustomController@showkawasan')->name('customs.showkawasan');
	Route::get('/admin/custom/showtipe', 'CustomController@showtipe')->name('customs.showtipe');

	Route::get('/admin/report/index', 'ReportController@index');
	Route::get('/admin/report/excel', 'ReportController@excel');
});

Route::get('/logout-admin' , 'Auth\AdminLoginController@logout')->name('admin.logout');

Route::get('/sop_view' , 'ViewSOPController@index')->name('sop_view');