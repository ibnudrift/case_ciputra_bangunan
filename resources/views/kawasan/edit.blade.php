@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Kawasan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kawasan</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">

        <div class="box">
         <div class="box-header">
            <h4>Update Kawasan - {{ $filter_proyek->nama }}</h4>
         </div>
         <div class="box-body">

            @if(isset($kawasan))
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/backend/admin/kawasan/' . $kawasan->id) }}" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">
                {{ csrf_field() }}
                <input type="hidden" name="proyek_id" value="{{ old('proyek_id', $kawasan->proyek_id) }}">
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title" class="col-md-4 control-label">Nama Kawasan</label>
                    <div class="col-md-6">
                        <input id="title" type="text" class="form-control" name="title" value="{{ old('title', $kawasan->title) }}" required="required">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
         
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i> Simpan
                        </button>
                         <a href="{{ URL::route('admin.searchkawasan') }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Cancel</a>
                    </div>
                </div>
            </form>
            @endif

            
         </div>
        </div>

 

     </div>
 </div>
<hr>
 
@endsection