@extends('template')
 
@section('content')
 
<h3>Data Karyawan</h3>
<hr>
 
@if(isset($karyawan))
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/karyawan/' . $karyawan->id) }}">
        <input name="_method" type="hidden" value="PATCH">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('nama_lengkap') ? ' has-error' : '' }}">
            <label for="nama_lengkap" class="col-md-4 control-label">Nama</label>
            <div class="col-md-6">
                <input id="nama_lengkap" type="text" class="form-control" name="nama_lengkap" value="{{ old('nama_lengkap', $karyawan->nama_lengkap) }}" required="required">
                @if ($errors->has('nama_lengkap'))
                <span class="help-block">
                    <strong>{{ $errors->first('nama_lengkap') }}</strong>
                </span>
                @endif
            </div>
        </div>
 
        <div class="form-group{{ $errors->has('NIK') ? ' has-error' : '' }}">
            <label for="NIK" class="col-md-4 control-label">NIK</label>
            <div class="col-md-6">
                <input id="NIK" type="text" class="form-control" name="NIK" value="{{ old('NIK', $karyawan->NIK) }}" required="required">
                @if ($errors->has('NIK'))
                <span class="help-block">
                    <strong>{{ $errors->first('NIK') }}</strong>
                </span>
                @endif
            </div>
        </div>
 
        <div class="form-group{{ $errors->has('jabatan') ? ' has-error' : '' }}">
            <label for="jabatan" class="col-md-4 control-label">Jabatan</label>
            <div class="col-md-6">
                <input id="jabatan" type="text" class="form-control" name="jabatan" value="{{ old('jabatan', $karyawan->Jabatan) }}" required="required">
                @if ($errors->has('jabatan'))
                <span class="help-block">
                    <strong>{{ $errors->first('jabatan') }}</strong>
                </span>
                @endif
            </div>
        </div>
 
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-user"></i> Simpan
                </button>
            </div>
        </div>
    </form>
 
@endif
 
 
@stop