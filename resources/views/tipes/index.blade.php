@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Kawasan > Tipe
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{ route('admin.searchkawasan', ['proyek'=> $filter_proyek->id, 'kawasan'=> $filter_kawasan->kawasan_id]) }}">Kawasan {{ $filter_proyek->nama }}</a></li>
        <li class="active">Tipe Unit</li> 
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">
        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif 
        <div class="box">
         <div class="box-header">
            <div class="row">
              <div class="col-md-8">
                <h3>{{ $filter_proyek->nama }} > {{ $filter_kawasan->title }} > Tipe</h3>
                <a href="{{ URL::route('tipes.create', ['proyek'=> $filter_proyek->id, 'kawasan'=> $filter_kawasan->id ]) }}" class="btn btn-primary">Tambah Tipe</a>

              </div>
              <div class="col-md-4">
                &nbsp;
              </div>
            </div>
         </div>
         <div class="box-body">
             @if(count($tipes) > 0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($tipes as $data)
                        <tr>
                            <td>{{ $data->nama }}</td>
                            <td>
                                <a href="{{ URL::route('tipes.edit',$data->id) }}" class="btn btn-info"></i> Edit</a>
                                <form action="{{action('TipesController@destroy', $data->id)}}" method="post" class="hapus" style="display:inline">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                
                <?php echo $tipes->render(); ?>
                <div style="clear: both; height: 3rem;"></div>
            @else
              belum ada data
            @endif
         </div>
         </div>
     </div>
 </div>
<hr>
 
@endsection