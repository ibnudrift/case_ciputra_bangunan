@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Tipe
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Kawasan</a></li>
        <li class="active">Tipe</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">

        <div class="box">
         <div class="box-header">
            <h4><small><b>Tambah Tipe</b></small><br>{{ $filter_proyek->nama }} > {{ $filter_kawasan->title }}</h4>
         </div>
         <div class="box-body">

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/backend/admin/tipes') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="proyek_id" value="{{ $proyek }}">
                <input type="hidden" name="kawasan_id" value="{{ $kawasan }}">
                <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                    <label for="nama" class="col-md-4 control-label">Nama</label>
                    <div class="col-md-6">
                        <input id="nama" type="text" class="form-control" name="nama" value="{{ old('nama') }}" required="required">
                        @if ($errors->has('nama'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nama') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
         
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i> Simpan
                        </button>
                        <a href="{{ URL::route('admin.tipesindexs', ['proyek'=> $proyek, 'kawasan'=>$kawasan]) }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Cancel</a>
                    </div>
                </div>
            </form>

         </div>
        </div>

 

     </div>
 </div>
<hr>
 
@endsection