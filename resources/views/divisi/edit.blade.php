@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        PIC
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">PIC</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">

        <div class="box">
         <div class="box-header">
            <h4>Update PIC</h4>
         </div>
         <div class="box-body">

            @if(isset($divisi))
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/backend/admin/divisi/' . $divisi->id) }}">
                    <input name="_method" type="hidden" value="PATCH">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('name_departement') ? ' has-error' : '' }}">
                        <label for="name_departement" class="col-md-4 control-label">Nama Departemen</label>
                        <div class="col-md-6">
                            <input id="name_departement" type="text" class="form-control" name="name_departement" value="{{ old('name_departement', $divisi->name_departement) }}" required="required">
                            @if ($errors->has('name_departement'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name_departement') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
             
                    <div class="form-group{{ $errors->has('company') ? ' has-error' : '' }}">
                        <label for="company" class="col-md-4 control-label">Perusahaan</label>
                        <div class="col-md-6">
                            <input id="company" type="text" class="form-control" name="company" value="{{ old('company', $divisi->company) }}" required="required">
                            @if ($errors->has('company'))
                            <span class="help-block">
                                <strong>{{ $errors->first('company') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
             
                    <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                        <label for="city" class="col-md-4 control-label">Kota</label>
                        <div class="col-md-6">
                            <input id="city" type="text" class="form-control" name="city" value="{{ old('city', $divisi->city) }}" required="required">
                            @if ($errors->has('city'))
                            <span class="help-block">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
             
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user"></i> Simpan
                            </button>
                             <a href="{{ URL::route('divisi.index') }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Cancel</a>
                        </div>
                    </div>
                </form>
             
            @endif

            
         </div>
        </div>

 

     </div>
 </div>
<hr>
 
@endsection