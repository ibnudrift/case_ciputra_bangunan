@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        PIC
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">PIC</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">
        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif
        <div class="box">
         <div class="box-header">
            <h3>Data PIC</h3>
            <a href="{{ URL::route('divisi.create') }}" class="btn btn-primary">Tambah PIC</a>
         </div>
         <div class="box-body">
             @if(count($divisi) > 0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Nama Departemen</th>
                            <th>Kota</th>
                            <th>Perusahaan</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($divisi as $data)
                        <tr>
                            <td>{{ $data->name_departement }}</td>
                            <td>{{ $data->city }}</td>
                            <td>{{ $data->company }}</td>
                            <td>
                                <a href="{{ URL::route('divisi.edit',$data->id) }}" class="btn btn-info"></i> Edit</a>
                                <form action="{{action('DivisiController@destroy', $data->id)}}" method="post" class="hapus" style="display:inline">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
            belum ada data
            @endif
            <?php echo $divisi->render(); ?>
         </div>
         </div>
     </div>
 </div>
<hr>
 
@endsection