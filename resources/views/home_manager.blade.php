@extends('layouts.front')

@section('content-kategori-active')
{{-- <p><i class="icon-sarana"></i> &nbsp;DIVISI PRASARANA</p> --}}
@endsection

@section('content-sidebar-link')
{{-- 
<li>
    <a href="#"><i class="fas fa-book"></i> &nbsp;Daftar Isi Buku</a>
</li>
<li>
    <a href="#"><i class="fas fa-video"></i> &nbsp;Daftar Isi Video</a>
</li> 
--}}
@endsection

@section('content')
<div class="container">

    {{-- <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div> --}}

    <div class="row justify-content-center">
        <div class="col-md-11">
            {{-- start block list --}}
            
            {{-- end block list --}}
            <div class="clear"></div>
        </div>
    </div>    

    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                      Dashboard  
                    </div>
                    <div class="col-md-6 text-right">
                      @if (Auth::user()->role_member == '3')
                        <small>Auditor</small>
                      @else
                        <small>Manajemen: <b>{{ $mproyek }}</b></small>
                      @endif
                    </div>
                </div>
                </div>
                <div class="card-body p-5">
                    
                    <div class="mb-2 card">
                        <div class="card-body fixn_body">
                            <form class="form-inline" method="GET" action="{{ url('/home') }}">
                              @if (Auth::user()->role_member == '3')
                              <div class="form-group">
                                <label for="inputPassword6">Proyek</label>
                                <select name="proyeks" id="" class="form-control mx-sm-3">
                                    @foreach ($m_proyek as $key => $value)
                                        @if ($value->id == $nget_proyek)
                                            <option selected="selected" value="{{ $value->id }}">{{ $value->info }}</option>
                                        @else
                                            <option value="{{ $value->id }}">{{ $value->info }}</option>
                                        @endif
                                    @endforeach
                                </select>
                              </div>
                              @endif
                              <div class="form-group">
                                <label for="inputPassword6">PIC</label>
                                <select name="division" id="" class="form-control mx-sm-3">
                                    <option value="">-- Pilih PIC --</option>
                                    @foreach ($m_divisi as $key => $value)
                                        @if ($value->id == $nget_divisi)
                                            <option selected="selected" value="{{ $value->id }}">{{ $value->name_departement }}</option>
                                        @else
                                            <option value="{{ $value->id }}">{{ $value->name_departement }}</option>
                                        @endif
                                    @endforeach
                                </select>
                              </div>
                              <button type="submit" class="btn btn-default">SUBMIT</button>
                            </form>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="lists-shortcut table-responsive">
                        @if (count($n_model) > 0)
                        <table class="table">
                          <thead class="thead-light">
                            <tr>
                              {{-- <th scope="col">#</th> --}}
                              <th scope="col">OPEN</th>
                              <th scope="col">CLOSE</th>
                              <th scope="col">-</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              {{-- <th scope="row">-</th> --}}
                              {{-- <td>{{ $model[1] }}</td> --}}
                              <td>
                              @if (isset($model[1]))
                              {{ $model[1] }}
                              @else
                              0
                              @endif
                              </td>
                              <td>
                              @if (isset($model[2]))
                              {{ $model[2] }}
                              @else
                              0
                              @endif
                              </td>
                              @if ( isset($nget_divisi) )
                                  @if (Auth::user()->role_member == '3')
                                  <td><a href="{{ url('/case_list/indexs') }}?divisi={{ $nget_divisi }}&proyek={{ $nget_proyek }}"><i class="fa fa-table"></i> Lihat Data</a></td>
                                  @else
                                  <td><a href="{{ url('/case_list/indexs') }}?divisi={{ $nget_divisi }}"><i class="fa fa-table"></i> Lihat Data</a></td>
                                  @endif
                              @else
                                  {{-- false expr --}}
                              @endif
                            </tr>
                          </tbody>
                        </table>
                        @else
                            <h5>Belum ada data</h5>
                        @endif
                    </div>
                    <div class="clear clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .inner-bx-search{
        display: none !important;
        visibility: hidden;
    }
</style>
@endsection
