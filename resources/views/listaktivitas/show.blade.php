@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Log Aktivitas
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Log Aktivitas</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">
        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif
        <div class="box">
         <div class="box-header">
            <h3>Report Activity</h3>
         </div>
         <div class="box-body">
          
            <h4>@if ($proyek)
            {{ ucfirst(trans($proyek->nama)) }} > @endif {{ ucfirst(trans($member->name)) }}</h4>
            <table class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Panduan Teknik</th>
                  <th>Jumlah Membaca</th>
                  <th>Tanggal</th>
                  <th>-</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($data as $vdata)
                <tr>
                  <td>{{ $vdata->name_artikel }}</td>
                  <td>{{ $vdata->jumlah }}</td>
                  <td>{{ date('d-m-Y', strtotime($vdata->tanggal_input)) }}</td>
                  <td>
                    <a href="{{ URL::route('admin.showdetail', ['user_id' => $vdata->user_id, 'post_id' => $vdata->post_id]) }}" class="btn btn-info"></i> Detail</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>

            <div class="clear divider15"></div>
         </div>
         </div>
     </div>
 </div>
<hr>
 
@endsection