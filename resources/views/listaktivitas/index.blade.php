@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Log Aktivitas
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Log Aktivitas</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">
        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif
        <div class="box">
         <div class="box-header">
            <h3>Pilih Member</h3>
         </div>
         <div class="box-body">
            
            <form class="form-inline" role="form" method="post" action="{{ url('/backend/admin/logaktivitas/showdata') }}">
              {{ csrf_field() }}
              <div class="form-group">
                <label class="sr-only" for="exampleInputEmail2">Pilih Proyek</label>
                @if ($proyek)
                  <select name="user_id" id="" class="form-control changes_proyek">
                    <option value="">-- Pilih Proyek --</option>
                    @foreach ($proyek as $key => $data)
                    <option value="{{ $key }}">{{ $data }}</option>
                    @endforeach
                  </select>
                @endif
              </div>
              <div class="form-group">
                <label class="sr-only" for="exampleInputEmail2">Pilih Member</label>
                @if ($member)
                  <select name="user_id" id="" class="form-control changes_memberP">
                    <option value="">-- Pilih User --</option>
                    @foreach ($member as $key => $data)
                    <option value="{{ $key }}">{{ $data }}</option>
                    @endforeach
                  </select>
                @endif
              </div>
              <button class="btn btn-primary">SUBMIT</button>
            </form>

            <div class="clear divider15"></div>
         </div>
         </div>
     </div>
 </div>
<hr>
 
@endsection