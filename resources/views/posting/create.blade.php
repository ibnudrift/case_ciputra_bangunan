@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Data Arsip
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Arsip</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">

        <div class="box">
         <div class="box-header">
            <h4>Tambah Data Arsip</h4>
         </div>
         <div class="box-body">

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/backend/admin/posting') }}">
                {{ csrf_field() }}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title" class="col-md-4 control-label">Judul</label>
                    <div class="col-md-6">
                        <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required="required">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
         
                <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                    <label for="category_id" class="col-md-4 control-label">Kategori</label>
                    <div class="col-md-6">
                        <select name="category_id" id="" class="form-control" required="required">
                            <option value="0">-- Pilih Kategori --</option>
                            @foreach ($kategori as $key => $data_kat)
                                @if ($key == old('category_id'))
                                <option selected="selected" value="{{ $key }}">{{ $data_kat }}</option>
                                @else
                                <option value="{{ $key }}">{{ $data_kat }}</option>
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('category_id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('category_id') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                {{-- <div class="form-group{{ $errors->has('tgroup') ? ' has-error' : '' }}">
                    <label for="tgroup" class="col-md-4 control-label">Group Arsip</label>
                    <div class="col-md-6">
                        <select name="tgroup" id="" class="form-control">
                            <option value="0">-- Pilih Group --</option>
                            @foreach ($tgroup as $keys => $data_nm)
                                @if ($keys == old('tgroup'))
                                <option selected="selected" value="{{ $keys }}">{{ $data_nm }}</option>
                                @else
                                <option value="{{ $keys }}">{{ $data_nm }}</option>
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('tgroup'))
                        <span class="help-block">
                            <strong>{{ $errors->first('tgroup') }}</strong>
                        </span>
                        @endif
                    </div>
                </div> --}}

                <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                    <label for="content" class="col-md-4 control-label">Content</label>
                    <div class="col-md-6">
                        <textarea name="content" id="content" class="form-control summernote" rows="7" required="required">{{ old('content') }}</textarea>
                        @if ($errors->has('content'))
                        <span class="help-block">
                            <strong>{{ $errors->first('content') }}</strong>
                        </span>
                        @endif
                    </div>
                </div> 

                <div class="form-group{{ $errors->has('video') ? ' has-error' : '' }}">
                    <label for="video" class="col-md-4 control-label">Url Video</label>
                    <div class="col-md-6">
                        <input id="video" type="text" class="form-control" name="video" value="{{ old('video') }}">
                        @if ($errors->has('video'))
                        <span class="help-block">
                            <strong>{{ $errors->first('video') }}</strong>
                        </span>
                        @endif
                    </div>
                </div> 

                <div class="form-group{{ $errors->has('is_video') ? ' has-error' : '' }}">
                    <label for="is_video" class="col-md-4 control-label">Type Video</label>
                    <div class="col-md-6">
                        <input id="is_video" type="checkbox" name="is_video" value="1"> Video Data
                        @if ($errors->has('is_video'))
                        <span class="help-block">
                            <strong>{{ $errors->first('is_video') }}</strong>
                        </span>
                        @endif
                    </div>
                </div> 
         
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i> Simpan
                        </button>
                        <a href="{{ URL::route('posting.index') }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Cancel</a>
                    </div>
                </div>
            </form>

         </div>
        </div>

 

     </div>
 </div>
<hr>

@endsection