@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Data Arsip
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Arsip</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">
        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif
        <div class="box">
         <div class="box-header">
          <div class="sfilters">
          <div class="row">
            <div class="col-md-7">
              <div class="grp_data mt-20">
                {{-- {{ URL::route('posting.index', ['kategori' => $datagrp->id]) }} --}}
                <form class="form-inline" method="GET" action="{{ URL::route('posting.index') }}">
                  <div class="form-group">
                    <label for="exampleInputName2">Kategori&nbsp;</label>
                    <select name="kategori" class="form-control tops_selectkategori" id="">
                      @foreach ($allgroup as $key => $element)
                        <option @if ($ac_group == $key) selected="selected" @endif value="{{ $key }}">{{ $element }}</option>
                      @endforeach
                    </select>
                  </div>
                  <button type="submit" class="btn btn-default">Submit</button>
                </form>

              </div>
            </div>
            <div class="col-md-5">
              <form class="form-inline frm_search" method="GET" action="{{ URL::route('posting.index') }}">
                <div class="form-group">
                  <label class="sr-only" for="exampleInputEmail3">Search</label>
                  <input type="text" class="form-control" name="search" id="exampleInputEmail3" value="{{ $ac_search }}" placeholder="Search...">
                </div>
                <button type="submit" class="btn btn-default">Search</button>
              </form>
            </div>
          </div>
          </div>
          <a href="{{ URL::route('posting.create') }}" class="btn btn-primary">Tambah Data Arsip</a>
         </div>
         <div class="box-body">

             @if(count($posts) > 0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Judul</th>
                            <th>Kategori</th>
                            <th>Terbaca</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($posts as $data)
                        <tr>
                            <td>{{ $data->title }}</td>
                            <td>{{ $data->names_category }}</td>
                            <td>{{ $data->views }}</td>
                            <td>
                                <a href="{{ URL::route('posting.edit',$data->id) }}" class="btn btn-info"></i> Edit</a>
                                <form action="{{action('PostingController@destroy', $data->id)}}" method="post" class="hapus" style="display:inline">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
            belum ada data
            @endif
            <?php echo $posts->render(); ?>
         </div>
         </div>
     </div>
 </div>
<hr>
 
<style>
  .sfilters{
    border-top: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
    margin-bottom: 10px;
  }
  .grp_data.mt-20{
    margin-top: 10px;
    margin-bottom: 10px;
    /*float: right;*/
  }
  form.frm_search{
    margin-bottom: 0; margin-top: 10px;
    float: right;
  }
  select.tops_selectkategori{
    max-width: 290px;
    display: inline-block;
  }
  button{
    border-radius: 0px;
  }
</style>
@endsection