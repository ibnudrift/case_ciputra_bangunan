<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('asset/images/dumy-users.png') }}" class="img-fluid d-block img-circle" alt="Logo Backend">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->nama}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      {{-- <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
       --}}

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>


        <li><a href="{{url('/backend')}}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>

        {{-- <li><a href="{{url('/backend/admin/category/index')}}"><i class="fa fa-cog"></i> <span>Kategori Case</span></a></li> --}}
        <li><a href="{{ url('/backend/admin/cases') }}"><i class="fa fa-book"></i> <span>Master Data Case</span></a></li>
        
        <li><a href="{{ url('/backend/admin/arsip/list') }}"><i class="fa fa-book"></i> <span>Case Arsip</span></a></li>
        
        {{-- <li><a href="{{ url('/backend/admin/report/index') }}"><i class="fa fa-cog"></i> <span>Report Data Case</span></a></li> --}}

        {{-- <li><a href="{{ url('/backend/admin/subjectc') }}"><i class="fa fa-cog"></i> <span>Master Subjects</span></a></li> --}}
        {{-- <li><a href="{{ url('/backend/admin/kawasan') }}"><i class="fa fa-folder"></i> <span>Master Kawasan</span></a></li>  --}}

        <li><a href="{{ url('/backend/admin/blok') }}"><i class="fa fa-folder"></i> <span>Master Blok</span></a></li> 

        <li>&nbsp;</li>
        {{-- <li><a href="{{url('/backend/admin/logaktivitas')}}"><i class="fa fa-history"></i> <span>Log Aktivitas</span></a></li> --}}
        {{-- <li><a href="{{url('/backend/admin/setuplog')}}"><i class="fa fa-cog"></i> <span>Status Log Aktivitas</span></a></li> --}}

        <li><a href="{{url('/backend/admin/member')}}"><i class="fa fa-users"></i> <span>Kontraktor</span></a></li>
        {{-- <li><a href="{{url('/backend/admin/divisi')}}"><i class="fa fa-bookmark"></i> <span>PIC</span></a></li> --}}
        {{-- <li><a href="{{url('/backend/admin/proyek')}}"><i class="fa fa-tag"></i> <span>Proyek</span></a></li> --}}
        <li><a href="{{url('/backend/admin/admins')}}"><i class="fa fa-key"></i> <span>List Admin</span></a></li>

        
    </section>
    <!-- /.sidebar -->
  </aside>
