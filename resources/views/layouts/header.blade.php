 <header class="main-header">
    <!-- Logo -->
    <a href="{{ route('admin.dashboard') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      {{-- <span class="logo-mini"><b>de'</b>AR</span> --}}
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Citraland Tallasa</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    @include('layouts.navigation')
  </header>