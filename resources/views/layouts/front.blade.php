<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
    
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="{{ asset('asset/css/style2.css') }}">

    <link rel="stylesheet" href="{{ asset('asset/css/styles.css') }}">

    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
        // document.addEventListener('contextmenu', event => event.preventDefault());
        // $(document).ready(function () {
        //     $('body').bind('cut copy paste', function (e) {
        //         e.preventDefault();
        //     });
        // });
    </script>
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript">
        $.noConflict();
        jQuery(function($){

            $( "#datepicker, .datepicker" ).datepicker({
                dateFormat: 'dd-M-yy',
                changeMonth: true,
                changeYear: true,
                minDate: new Date()
            });
            
        });
    </script>
</head>
<body>
    <div class="wrapper">

        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3><a href="{{ URL::route('home') }}" class="logo"><img src="{{ asset('asset/images/pic-logo-centers-ciputra.png') }}" alt="" class="img-fluid mx-auto d-block"></a></h3>
            </div>

            <ul class="list-unstyled components">
                @yield('content-kategori-active')

                <li>
                    <a href="{{ URL::route('home') }}"><i class="fas fa-home"></i> &nbsp;Home</a>
                </li>
                @yield('content-sidebar-link')
            </ul>

        </nav>

        <!-- Page Content  -->
        <div id="content">
            <div class="d-block d-sm-none">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                  <a class="navbar-brand" href="#">Ciputra | Audit Case</a>
                  <button class="navbar-toggler" type="button" id="sidebarCollapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                  </button>
                </nav>
            </div>

            <div class="d-none d-sm-block d-md-block d-lg-none views_menumobile">
            <button type="button" id="sidebarCollapse" class="btn btn-light">
                <i class="fa fa-bar"></i> &nbsp;MENU
            </button>
            </div>

            <div class="blocks-tops-search">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="row">
                        <div class="col-md-6">
                            {{-- <div class="inner-bx-search">
                                <form class="form-inline" method="get" action="{{ url('search') }}">
                                  <label class="sr-only" for="inlineFormInputName2">Search</label>
                                  <input type="text" class="form-control mb-2 mr-sm-2" name="search" id="search_text" placeholder="Search..">
                                  <button type="submit" name="searcin" class="btn btn-success mb-2">Search</button>
                              </form>
                            </div> --}}
                        </div>
                        <div class="col-md-6">
                            <ul class="nav navbar-nav float-right">
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Selamat Datang, <b>{{ Auth::user()->name }}</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">{{ __('Logout') }} &nbsp;<i class="fas fa-sign-out-alt"></i></a>
                                </li>
                            </ul>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            <div class="clear clearfix"></div>
                        </div>
                    </div>

            </nav>

                <div class="clear"></div>
            </div>


            <div class="block-right-content">
                @yield('content')
                <div class="clear"></div>
            </div>

            <div class="clear clearfix"></div>
        </div>
    </div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>


    <script type="text/javascript">
        $.noConflict();
       jQuery(document).ready(function($) {
        src = "{{ route('searchajax') }}";
         $("#search_text").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: src,
                    dataType: "json",
                    data: {
                        term : request.term
                    },
                    success: function(data) {
                        response(data);
                       
                    }
                });
            },
            minLength: 3,
           
        });

        // var link_glist = "{{ route('list') }}";
        // $('.onselects_change_group').change(function(e){
        //   var s_kate = $(this).attr('data-kategori');
        //   var is_url = link_glist +'?id='+s_kate+'&group='+ $(this).val();
        //   window.open(is_url, '_self');
        //   e.preventDefault();
        // });

    });
    </script>
</body>
</html>
