@extends('layouts.master')
@section('content-header')
<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
@endsection
@section('data-content')
<div class="row" >
  <div class="col-md-12">
    <div class="clear divider25"></div>
    <div class="clear divider15"></div>

     {{-- <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-gear-outline"></i></span>

            <div class="info-box-content">
              <a href="{{url('/backend/admin/cases')}}"><span class="info-box-text">Data</span>
              <span class="info-box-number">List Master Case</span></a>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>
            <div class="info-box-content">
              <a href="{{url('/backend/admin/logaktivitas')}}"><span class="info-box-text">Data</span>
              <span class="info-box-number">Log Aktivitas</span></a>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div> --}}
      {{-- End list thumbs --}}
    
    <div class="outers-block-wrappergm">
      <div class="row">
        <div class="col-md-10">

          <div>
            <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#kontraktor" aria-controls="kontraktor" role="tab" data-toggle="tab"><strong>KONTRAKTOR</strong></a></li>
              <li role="presentation"><a href="#supervisors" aria-controls="supervisors" role="tab" data-toggle="tab"><strong>SUPERVISOR</strong></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active" id="kontraktor">

                <div class="card">
                  <div class="card-header">
                    Dashboard Report <strong>Kontraktor</strong>
                  </div>
                  <div class="card-body p-5">
                    
                    <div class="lists-shortcut table-responsive">
                    @if (count($n_model) > 0)
                    <table class="table table-bordered">
                      <thead class="thead-light">
                        <tr>
                          <th scope="col">KONTRAKTOR</th>
                          <th scope="col">OPEN</th>
                          <th scope="col">CLOSE</th>
                          <th scope="col">-</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php ($full_open = 0)
                        @php ($full_close = 0)

                        @foreach ($n_model as $key => $val_element)
                        @php ( $full_open = $full_open + $val_element['open'] )
                        @php ( $full_close = $full_close + $val_element['close'] )
                        <tr>
                          <td>{{ $val_element['divisi_name'] }}</td>
                          <td>{{ $val_element['open'] }}</td>
                          <td>{{ $val_element['close'] }}</td>
                          <td>
                          @if ($val_element['total_data'] > 0)
                            <a href="{{ url('/backend/admin/manager/list') }}?divisi={{ $val_element['divisi_id'] }}">
                          @else
                            <a href="#">
                          @endif
                          <i class="fa fa-table"></i> Lihat Data</a></td>
                        </tr>
                        @endforeach
                        <tr>
                            <td><b>Total</b></td>
                            <td><b>{{ $full_open }}</b></td>
                            <td><b>{{ $full_close }}</b></td>
                            <td>-</td>
                        </tr>
                      </tbody>
                    </table>

                    @else
                        <h5>Belum ada data</h5>
                    @endif
                </div>

                  </div>
                </div> 

                {{-- end panel tab --}}
              </div>
              <div role="tabpanel" class="tab-pane" id="supervisors">
                <div class="card">
                  <div class="card-header">
                    Dashboard Report <strong>SUPERVISOR</strong>
                  </div>
                  <div class="card-body p-5">
                    
                    <div class="lists-shortcut table-responsive">
                    @if (count($n_model_spv) > 0)
                    <table class="table table-bordered">
                      <thead class="thead-light">
                        <tr>
                          <th scope="col">SUPERVISOR</th>
                          <th scope="col">OPEN</th>
                          <th scope="col">CLOSE</th>
                          <th scope="col">-</th>
                        </tr>
                      </thead>
                      <tbody>
                        @php ($full_open2 = 0)
                        @php ($full_close2 = 0)

                        @foreach ($n_model_spv as $key => $val_element)
                        @php ( $full_open2 = $full_open2 + $val_element['open'] )
                        @php ( $full_close2 = $full_close2 + $val_element['close'] )
                        <tr>
                          <td>{{ $val_element['divisi_name'] }}</td>
                          <td>{{ $val_element['open'] }}</td>
                          <td>{{ $val_element['close'] }}</td>
                          <td>
                          @if ($val_element['total_data'] > 0)
                            <a href="{{ url('/backend/admin/manager/list2') }}?spv_id={{ $val_element['spv_id'] }}">
                          @else
                            <a href="#">
                          @endif
                          <i class="fa fa-table"></i> Lihat Data</a></td>
                        </tr>
                        @endforeach
                        <tr>
                            <td><b>Total</b></td>
                            <td><b>{{ $full_open2 }}</b></td>
                            <td><b>{{ $full_close2 }}</b></td>
                            <td>-</td>
                        </tr>
                      </tbody>
                    </table>

                    @else
                        <h5>Belum ada data</h5>
                    @endif
                </div>

                  </div>
                </div> 
                {{-- end panel tab --}}
              </div>
            </div>

          </div>
          {{-- end tab data      --}}

        </div>
        <div class="col-md-2">

        </div>
      </div>

      <div class="clear clearfix"></div>
    </div>


    </div>
</div>
      <!-- /.row (main row) -->
@endsection