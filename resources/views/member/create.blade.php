@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Kontraktor
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kontraktor</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">

        <div class="box">
         <div class="box-header">
            <h4>Tambah Kontraktor</h4>
         </div>
         <div class="box-body">

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/backend/admin/member') }}">
                {{ csrf_field() }}

                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="col-md-4 control-label">Kontraktor</label>
                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required="required" placeholder="Nama Kontraktor">
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
         
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">Username</label>
                    <div class="col-md-6">
                        <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Username" required="required">
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>
                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" required="required">
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                {{-- <div class="form-group{{ $errors->has('nik') ? ' has-error' : '' }}">
                    <label for="nik" class="col-md-4 control-label">Nomor Karyawan</label>
                    <div class="col-md-6">
                        <input id="nik" type="text" class="form-control" name="nik" value="{{ old('nik') }}" required="required">
                        @if ($errors->has('nik'))
                        <span class="help-block">
                            <strong>{{ $errors->first('nik') }}</strong>
                        </span>
                        @endif
                    </div>
                </div> --}}

                <input type="hidden" name="proyek_id" value="1">
                {{-- <div class="form-group {{ $errors->has('division') ? ' has-error' : '' }}">
                    <label for="division" class="col-md-4 control-label">Supervisor</label>
                    <div class="col-md-6">
                        <select name="division" id="" class="form-control" required="required">
                            <option value="0">-- Pilih Supervisor --</option>
                            @foreach ($admins as $key => $v_admin)
                                <option value="{{ $key }}">{{ $v_admin }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('division'))
                        <span class="help-block">
                            <strong>{{ $errors->first('division') }}</strong>
                        </span>
                        @endif
                    </div>
                </div> --}}
                
                {{-- <div class="form-group {{ $errors->has('division') ? ' has-error' : '' }}">
                    <label for="division" class="col-md-4 control-label">PIC</label>
                    <div class="col-md-6">
                        <select name="division" id="" class="form-control" required="required">
                            <option value="0">-- Pilih PIC --</option>
                            @foreach ($divisi as $key => $division)
                                @if ($key == old('division'))
                                <option selected="selected" value="{{ $key }}">{{ $division }}</option>
                                @else
                                <option value="{{ $key }}">{{ $division }}</option>
                                @endif
                            @endforeach
                        </select>
                        @if ($errors->has('division'))
                        <span class="help-block">
                            <strong>{{ $errors->first('division') }}</strong>
                        </span>
                        @endif
                    </div>
                </div> --}}

                <div class="form-group {{ $errors->has('role_member') ? ' has-error' : '' }}">
                    <label for="role_member" class="col-md-4 control-label">Role User</label>
                    <div class="col-md-6">
                        <select name="role_member" id="" class="form-control" required="required">
                            <option value="4">Manajemen</option>
                            <option value="1">Kontraktor</option>
                            {{-- <option value="2">Manager Divisi</option> --}}
                            {{-- <option value="3">Auditor</option> --}}
                        </select>
                        @if ($errors->has('role_member'))
                        <span class="help-block">
                            <strong>{{ $errors->first('role_member') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i> Simpan
                        </button>
                        <a href="{{ URL::route('member.index') }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Cancel</a>
                    </div>
                </div>
            </form>

         </div>
        </div>

 

     </div>
 </div>
<hr>
 
@endsection