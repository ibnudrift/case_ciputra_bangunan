@extends('layouts.master')
@section('content-header')
<section class="content-header">
      <h1>
        Admin
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Admin</li>
      </ol>
    </section>
@endsection
@section('data-content')
  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Admin</h3>
            </div>
            @if(count($errors) > 0)
              @foreach($errors->all() as $error)
                <div class = "alert alert-danger">
                  {{$error}}
                </div>
              @endforeach 
            @endif

            @if(session('info'))
              <div class = "alert alert-success">
                {{session('info')}}
              </div>
            @endif
            <div class="box-header">
              <h4 class="box-title">
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-info">
                  Tambah Data
                </button>
              </h4>
            </div>
            <!------ modalll
            -->
            
            <form action = "{{url('/backend/admin/admins/postInsert')}}" method="POST" >
              {{csrf_field()}}
              <div class="modal modal-info fade" id="modal-info">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title">Tambah Data Admin</h4>
                        </div>

                        <div class="modal-body">
                          <label>Posisi</label>
                          <select name="role_type" id="" class="form-control" required="required">
                            <option value="">-- Pilih Posisi --</option>
                            @foreach ($position as $key => $data_names)
                                <option value="{{ $key }}">{{ $data_names }}</option>
                            @endforeach
                          </select>
                          <label>Nama Karyawan</label>
                          <input class="form-control" type="text" placeholder="Username / Nama Karyawan" name ="username">
                          <label>Password</label>
                          <input class="form-control" type="password" placeholder="Password" name ="password">
                          <label>Email</label>
                          <input class="form-control" type="text" placeholder="Email" name ="email">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Save changes</button>
                          </a>
                        </div>
                      </div>
                      <!-- /.modal-content -->
                    </div>
              <!-- /.modal-dialog -->
              </div>
            </form>

            <!-- /.box-header -->
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-12">
                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                      <thead>
                        <tr role="row">
                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" style="width: 130px;">Posisi</th>
                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Nama admin: activate to sort column descending" style="width: 174px;">Nama Karyawan</th>
                        {{-- <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending" style="width: 215px;">Username</th> --}}
                        {{-- <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Password: activate to sort column ascending" style="width: 215px;">Password</th> --}}
                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Email: activate to sort column ascending" style="width: 215px;">Email</th>
                        <th  tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  style="width: 149px;">Edit</th>
                        <th  tabindex="0" aria-controls="example1" rowspan="1" colspan="1"  style="width: 149px;">Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                      @if(count($admin) > 0)
                        @foreach($admin as $key => $admins)
                            <tr role="row">
                            @if ($admins->role_type)
                              <td class="sorting_1">{{ $admins->role_type }}</td>
                            @else
                              <td class="sorting_1">&nbsp;</td>
                            @endif
                            <td class="sorting_1">{{$admins->username}}</td>
                            <td>{{$admins->email}}</td>
                            {{-- <td>{{$admins->password}}</td> --}}
                            {{-- <td>{{$admins->email}}</td> --}}
                            <td><a href='{{url("backend/admin/admins/edit/{$admins->id_admin}")}}'><button type="button" class="btn btn-block btn-warning" >Edit</button></a></td>
                            <td><a href ='{{url("backend/admin/admins/delete/{$admins->id_admin}")}}'><button type="button" class="btn btn-block btn-danger">Delete</button></a></td>
                          </tr>
                        @endforeach 
                      @endif
                      
                      </tbody>
                      <!--
                      <tfoot>
                        <tr><th rowspan="1" colspan="1">Rendering engine</th><th rowspan="1" colspan="1">Browser</th><th rowspan="1" colspan="1">Platform(s)</th><th rowspan="1" colspan="1">Engine version</th><th rowspan="1" colspan="1">CSS grade</th></tr>
                      </tfoot>
                      -->
                    </table>
              </div>
              </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>

  <script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@endsection
