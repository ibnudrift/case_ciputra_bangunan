@extends('layouts.master')
@section('content-header')
<section class="content-header">
      <h1>
        Edit Admin
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li >Admin</li>
        <li class="active">Edit Admin</li>
      </ol>
    </section>
@endsection
@section('data-content')
<form action="{{url('/backend/admin/admins/update' , array($adminedit->id_admin))}}" method="POST">
{{ csrf_field() }}
  @if(count($errors) > 0)
    @foreach($errors->all() as $error)
      <div class = "alert alert-danger">
        {{$error}}
      </div>
    @endforeach 
  @endif

  {{-- <label>Nama Lengkap</label>
  <input class="form-control" type="text" placeholder="Nama Lengkap" name ="nama" value='{{$adminedit->nama}}'> --}}
  
  <label>Posisi</label>
  <select name="role_type" class="form-control" required="required">
      <option value="0">-- Pilih Posisi --</option>
      @foreach ($position as $key => $data_names)
          @if ($key == old('role_type', $adminedit->role_type))
          <option selected="selected" value="{{ $key }}">{{ $data_names }}</option>
          @else
          <option value="{{ $key }}">{{ $data_names }}</option>
          @endif
      @endforeach
  </select>

  <label>Nama Karyawan</label>
  <input class="form-control" type="text" placeholder="Keterangan" name ="username" value='{{$adminedit->username}}'>
  <label>Email</label>
  <input class="form-control" type="text" placeholder="Keterangan" name ="email" value='{{$adminedit->email}}'>
  <label for="password">Password</label>
  <input id="password" type="password" class="form-control" name="password" value="">
  @if ($errors->has('password'))
  <span class="help-block">
      <strong>{{ $errors->first('password') }}</strong>
  </span>
  @endif
  <span class="help-block">
      Isi password di atas apabila ingin merubah password anda.
  </span>
  
  <div class="divider15"></div>
  <a href ="{{url('/backend/admin/admins')}}" type="submit" class="btn btn-primary">Back</a>
  <button type="submit" class="btn btn-primary" >Save Changes</button>
</form>
@endsection