@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Blok Kavling
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Blok Kavling</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">

        <div class="box">
         <div class="box-header">
            <h4>Update Blok Kavling</h4>
         </div>
         <div class="box-body">

            @if(isset($blok))
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/backend/admin/blok/' . $blok->id) }}" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title" class="col-md-4 control-label">Title</label>
                    <div class="col-md-6">
                        <input id="title" type="text" class="form-control" name="title" value="{{ old('title', $subjects->title) }}" required="required">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
         
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i> Simpan
                        </button>
                         <a href="{{ URL::route('blok.index') }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Cancel</a>
                    </div>
                </div>
            </form>
            @endif

            
         </div>
        </div>

 

     </div>
 </div>
<hr>
 
@endsection