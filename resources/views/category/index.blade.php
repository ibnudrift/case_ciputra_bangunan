@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Kategori Case
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kategori Case</li>
      </ol>
    </section>
@endsection
@section('data-content')
<div class="row">
     <div class="col-xs-12">

        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif

        <div class="row">
          <div class="col-md-6">
            <div class="box">
               <div class="box-header">
                  <h3>Tambah Kategori Case</h3>
               </div>
               <div class="box-body">
                {!! Form::open(['route'=>'add.category', 'files' => true]) !!}

                @if ($message = Session::get('success'))
                  <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button> 
                        <strong>{{ $message }}</strong>
                  </div>
                @endif
                <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                  {!! Form::label('Title:') !!}
                  {!! Form::text('title', old('title'), ['class'=>'form-control','placeholder'=>'Enter Title']) !!}
                  <span class="text-danger">{{ $errors->first('title') }}</span>
                </div>
                <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : '' }}">
                  {!! Form::label('Category:') !!}
                  {!! Form::select('parent_id',$allCategories, old('parent_id'), ['class'=>'form-control', 'placeholder'=>'Select Category']) !!}
                  <span class="text-danger">{{ $errors->first('parent_id') }}</span>
                </div>
                <div class="form-group {{ $errors->has('sorting') ? 'has-error' : '' }}">
                  {!! Form::label('Urutan:') !!}
                  {!! Form::text('sorting', old('sorting'), ['class'=>'form-control','placeholder'=>'Enter Urutan']) !!}
                  <span class="text-danger">{{ $errors->first('sorting') }}</span>
                </div>
                {{-- <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                  {!! Form::label('image', 'Icon Kategori') !!}
                  {!! Form::file('image', ['class'=>'form-control']) !!}
                  <p class="help-block" style="font-size: 11px;">*) Note: picture size width: 140px x height: 140px</p>
                  <span class="text-danger">{{ $errors->first('image') }}</span>
                </div>
                 --}}
              <div class="form-group">
                  <button class="btn btn-success">Simpan</button>
                </div>
              {!! Form::close() !!}

                <div class="clear clearfix"></div>
               </div>
             </div>
          </div>
          <div class="col-md-6">
            <div class="box">
               <div class="box-header">
                  <h3>List Kategori Case</h3>
               </div>
               <div class="box-body">
                <div class="tree block-section">
                  <ul id="tree1">
                    @foreach($categories as $category)
                        <li>
                            <a href="{{ URL::route('category.edit', ['id' => $category->id]) }}">{{ $category->title }}</a> &nbsp;<a href="{{ URL::route('category.delete', ['id' => $category->id]) }}" class="delete" title="Hapus Data Category"><i class="fa fa-trash"></i></a>
                            @if(count($category->childs))
                                @include('category.manageChild', ['childs' => $category->childs])
                            @endif
                        </li>
                    @endforeach
                  </ul>
              </div>

               </div>
             </div>
          </div>
        </div>
        {{-- End rows snData --}}

     </div>
 </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="{{ asset('asset/js/treeview.js') }}"></script>

  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  <script type="text/javascript">
    jQuery.noConflict();
    jQuery(document).ready(function($)
    {
        $(document).on('click','.block-section.tree ul li a.delete',function() {
          if(!confirm('Are you sure you want to delete this item?')) return false;
          // $.ajax({
          //     type:'GET',
          //     url:$(this).attr('href'),
          //     // data: {ajax: 'delete'},
          //     success:function(data) {
          //       swal("Data berhasil di hapus!.");

          //       setTimeout(function(){ 
          //         location.reload();
          //       }, 1800);
          //     },
          // });
          return true;
      });
  });
  </script>
@endsection