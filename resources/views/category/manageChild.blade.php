<ul>
@foreach($childs as $child)
	<li>
	    <a href="{{ URL::route('category.edit', ['id' => $child->id]) }}">{{ $child->title }}</a> &nbsp;<a href="{{ URL::route('category.delete', ['id' => $child->id]) }}" class="delete" title="Hapus Data Category"><i class="fa fa-trash"></i></a>
	@if(count($child->childs))
            @include('category.manageChild',['childs' => $child->childs])
        @endif
</li>
@endforeach
</ul>