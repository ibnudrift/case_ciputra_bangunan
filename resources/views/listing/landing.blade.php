@extends('layouts.front')

@section('content-kategori-active')
<p><i class="icon-sarana"></i> &nbsp;DIVISI {{ $parent_cat->title }}</p>
@endsection

@section('content-sidebar-link')

@endsection

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-11">
            
            {{-- Start block list --}}
             <div class="card default_card">
                <div class="card-header">
                    <div class="row no-gutters">
                        <div class="col-md-12">
                            <h5>DAFTAR ISI</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="clear height-5"></div>
                    {{-- Start body --}}
                    <div class="tree block-section pl-2 pb-3">
                      <ul id="tree1">
                        @foreach($kategori as $category)
                            <li>
                                <a href="{{ URL::route('list', ['id' => $category->id, 'parent'=> $parent_cat->id]) }}">{{ $category->title }}</a>
                                @if(count($category->childs))
                                    @include('listing.manageChild', ['childs' => $category->childs, 'parent_cat'=>$parent_cat->id])
                                @endif
                            </li>
                        @endforeach
                      </ul>
                  </div>
                    {{-- End Body --}}

                    <div class="clear"></div>
                </div>
            </div>
            {{-- End block list --}}

            <div class="clear"></div>
        </div>
    </div>    

</div>
@endsection
