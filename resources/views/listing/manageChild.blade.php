<ul>
@foreach($childs as $child)
	<li>
	    <a href="{{ URL::route('list', ['id' => $child->id, 'parent'=> $parent_cat]) }}">{{ $child->title }}</a>
	@if(count($child->childs))
            @include('listing.manageChild',['childs' => $child->childs])
        @endif
</li>
@endforeach
</ul>