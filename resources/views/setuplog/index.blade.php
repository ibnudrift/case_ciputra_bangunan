@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Setup Log
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Setup Log</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">
        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif
        <div class="box">
         <div class="box-header">
            <h3>Data Setup Log</h3>
            {{-- <a href="{{ URL::route('setuplog.create') }}" class="btn btn-primary">Tambah Setup Log</a> --}}
         </div>
         <div class="box-body">
             @if(count($setuplog) > 0)
                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Status</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($setuplog as $data)
                        <tr>
                            <td>{{ $data->logs }}</td>
                            <td>{{ ($data->status)? "Aktif": "Tidak Aktif" }}</td>
                            <td>
                                <a href="{{ URL::route('setuplog.edit',$data->id) }}" class="btn btn-info"></i> Edit</a>
                                <form action="{{action('SetuplogController@destroy', $data->id)}}" method="post" class="hapus" style="display:inline">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                {{-- <button class="btn btn-danger" type="submit">Delete</button> --}}
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
            belum ada data
            @endif
            <?php echo $setuplog->render(); ?>
         </div>
         </div>
     </div>
 </div>
<hr>
 
@endsection