@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Setup Log
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Setup Log</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">

        <div class="box">
         <div class="box-header">
            <h4>Update Setup Log</h4>
         </div>
         <div class="box-body">

            @if(isset($setuplog))
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/backend/admin/setuplog/' . $setuplog->id) }}">
                    <input name="_method" type="hidden" value="PATCH">
                    {{ csrf_field() }}

                    <div class="form-group {{ $errors->has('logs') ? ' has-error' : '' }}">
                    <label for="logs" class="col-md-4 control-label">Logs Name</label>
                    <div class="col-md-6">
                        <input id="logs" type="text" readonly="readonly" class="form-control" name="logs" value="{{ old('logs', $setuplog->logs) }}" >
                        @if ($errors->has('logs'))
                        <span class="help-block">
                            <strong>{{ $errors->first('logs') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    <label for="status" class="col-md-4 control-label">Status</label>
                    <div class="col-md-6">
                        <select name="status" id="status" class="form-control">
                            <option value="1" @if ($setuplog->status == 1) selected="selected" @endif>Ya</option>
                            <option value="0" @if ($setuplog->status == 0) selected="selected" @endif>Tidak</option>
                        </select>
                        @if ($errors->has('status'))
                        <span class="help-block">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                        @endif
                    </div>
                </div> 
             
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-btn fa-user"></i> Simpan
                            </button>
                             <a href="{{ URL::route('setuplog.index') }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Cancel</a>
                        </div>
                    </div>
                </form>
             
            @endif

            
         </div>
        </div>

 

     </div>
 </div>
<hr>
 
@endsection