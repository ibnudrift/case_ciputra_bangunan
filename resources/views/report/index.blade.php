@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Report Case
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Report Case Audit Lapangan</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">
        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif
        <div class="box">
         <div class="box-header">
            <h3>Report Case Audit Lapangan</h3>
            {{-- <a href="{{ URL::route('proyek.create') }}" class="btn btn-primary">Tambah Proyek</a> --}}
         </div>
         <div class="box-body">
             {{-- @if(count($proyek) > 0) --}}
                <table class="table">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Foto Sebelumnya</th>
                            <th>Keterangan</th>
                            {{-- <th>Penyebab</th> --}}
                            <th>Instruksi</th>
                            <th>Foto Hasil Perbaikan</th>
                            {{-- <th>Kontraktor / Member</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                      @if(count($model) > 0)
                        @foreach ($model as $data)
                        <tr>
                          <td>{{ $data->no_case }}</td>
                          <td>
                            <div class="well-pic" style="max-width: 150px;"><img class="img img-responsive" src="{{ URL::to('/upload/case/'. $data->foto_sebelum )}}" alt=""></div>
                          </td>
                          <td>{{ $data->contents }}</td>
                          <td>{{ $data->penyebab }}</td>
                          <td>{{ $data->cara_perbaikan }}</td>
                          <td>
                            <div class="well-pic" style="max-width: 150px;"><img class="img img-responsive" src="{{ URL::to('/upload/test_foto/'. $data->foto_sesudah )}}" alt=""></div>
                          </td>
                          {{-- <td></td> --}}
                        </tr>
                        @endforeach
                      @endif
                    </tbody>
                </table>

                <div style="clear: both; height: 10px;"></div>
                <?php echo $model->render(); ?>
                
            {{-- @else
            belum ada data
            @endif --}}
         </div>
         </div>
     </div>
 </div>
<hr>
 
@endsection