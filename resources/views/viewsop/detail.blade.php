<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
    
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="{{ asset('asset/css/style2.css') }}">

    <link rel="stylesheet" href="{{ asset('asset/css/styles.css') }}">

    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>
</head>
<body>
    <div class="wrapper">

        <!-- Sidebar  -->
        <nav id="sidebar">
            <div class="sidebar-header">
                <h3><a href="#" class="logo"><img src="{{ asset('asset/images/pic-logo-centers-ciputra.png') }}" alt="" class="img-fluid mx-auto d-block"></a></h3>
            </div>

            <ul class="list-unstyled components">
                <p><i class="icon-sarana"></i> &nbsp;DIVISI {{ $kategori->title_kategori }}</p>
            </ul>

        </nav>

        <!-- Page Content  -->
        <div id="content">
            <div class="blocks-tops-search">
                <nav class="navbar navbar-expand-lg navbar-light bg-light" style="padding-top: 1em; padding-bottom:1em;">
                    <div class="row">
                        <div class="col-md-12 text-left">
                            <h6><b>CIPUTRA</b> | PANDUAN TEKNIK</h6>
                        </div>
                    </div>
                </nav>

                <div class="clear"></div>
            </div>


            <div class="block-right-content">
                
                {{-- start content --}}
                <div class="container">

                    <div class="row justify-content-center">
                        <div class="col-md-12">
                            
                            <nav aria-label="breadcrumb" class="blocks_breadcrumbs">
                              <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">{{ $data->title }}</li>
                              </ol>
                            </nav>

                            {{-- start block list --}}
                             <div class="card default_card">
                                <div class="card-body">
                                    <div class="clear height-5"></div>
                                    {{-- Start body --}}
                                    <div class="padding-def-content detail-content-post">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <h6 class="category">{{ $kategori->title_kategori }}</h6>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="text-right">
                                                <span class="dates"><i class="fa fa-calendar"></i> &nbsp;{{ date('d - m - Y', strtotime($data->created_at)) }}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear height-5"></div>
                                        <div class="block-titles">
                                            <h2 class="title">{{ $data->title }}</h2>
                                        </div>
                                        <div class="clear height-10"></div>
                                        @if ($data->video != '')
                                        <div class="frame_video">
                                            <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='{{ $data->video }}?rel=0&autoplay=1' frameborder='0' allowfullscreen></iframe></div>
                                        </div>
                                        @endif

                                        {!! $data->content !!}
                                        <div class="clear height-20"></div>
                                        <div class="dropdown-divider"></div>
                                        <div class="clear height-10"></div>
                                        <div class="blocks_back_article">
                                            <a href="{{ url('/list/' . '?id='.$kategori->id.'&type=list' ) }}" class="btn btn-link"><i class="fa fa-chevron-left"></i> &nbsp;Back</a>
                                        </div>
                                        <div class="clear height-30"></div>
                                        <div class="clear"></div>     
                                    </div>

                                    {{-- End Body --}}
                                    <div class="clear"></div>
                                </div>
                            </div>
                            {{-- end block list --}}

                            <div class="clear"></div>
                        </div>
                    </div>    

                </div>

                {{-- end content --}}
                <div class="clear"></div>
            </div>

            <div class="clear clearfix"></div>
        </div>
    </div>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
</body>
</html>
