@extends('layouts.appadmin')
@section('content')
<div class="container">

    <form class="form-signin" method="post" action="{{ route('admin.login.submit') }}">
      <img class="mb-4 d-block mx-auto logos_ciput" src="{{ asset('asset/images/logo-ciputra.png') }}" alt="logo ciputra">
      <h1 class="h3 mb-3 font-weight-normal">Bangunan Citraland Tallasa Login</h1>
      {{ csrf_field() }}
        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            <label for="usrname" class="sr-only" id="fontz">Username</label>
            <input id="username" type="text" class="form-control" name="email" value="{{ old('username') }}" placeholder="Username" required autofocus>
            @if ($errors->has('username'))
                <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="usrname" class="sr-only" id="fontz">Password</label>
            <input id="password" type="password" class="form-control" name="password" placeholder="Password" value="{{ old('password') }}" required autofocus>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>
      <div class="checkbox mb-3">
        <label>
          <input type="checkbox" value="remember-me"> Remember me
        </label>
      </div>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
    </form>

    <p class="mt-5 mb-3 text-muted">&copy; copyright 2020 - Citraland Tallasa City - Makassar</p>

    {{-- <div class="row d-none">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Admin Login</div>
                @php
                    //Auth::shouldUse('admin');
                    //dd(Auth::user('admin'));
                    //dd($errors);
                @endphp

                <div class="panel-body" id="containerz">
                    <form class="form-horizontal" method="POST" action="{{ route('admin.login.submit') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="usrname" class="col-md-4 control-label" id="fontz">Username</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="email" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label" id="fontz">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label id="fontz">
                                        <input  type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn">
                                    Login
                                </button>

                                <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> --}}
</div>

@endsection
