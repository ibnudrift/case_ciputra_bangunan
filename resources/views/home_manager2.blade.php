@extends('layouts.front')

@section('content-kategori-active')
{{-- <p><i class="icon-sarana"></i> &nbsp;DIVISI PRASARANA</p> --}}
@endsection

@section('content-sidebar-link')
@if ( Auth::user()->role_member == 1 )
<li>
    <a href="{{ url('/home') }}"><i class="fas fa-chevron-left"></i> &nbsp; Case Saya</a>
</li>
@endif
@endsection

@section('content')
<div class="container">

    {{-- <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div> --}}

    <div class="row justify-content-center">
        <div class="col-md-11">
            {{-- start block list --}}
            
            {{-- end block list --}}
            <div class="clear"></div>
        </div>
    </div>    

    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                      Dashboard  
                    </div>
                    <div class="col-md-6 text-right">
                        <small>Manajemen: <b>{{ $mproyek }}</b></small>
                    </div>
                </div>
                </div>
                <div class="card-body p-5">

                    <div class="lists-shortcut table-responsive">
                        @if (count($n_model) > 0)
                        <table class="table">
                          <thead class="thead-light">
                            <tr>
                              <th scope="col">KONTRAKTOR</th>
                              <th scope="col">OPEN</th>
                              <th scope="col">CLOSE</th>
                              <th scope="col">-</th>
                            </tr>
                          </thead>
                          <tbody>
                            @php ($full_open = 0)
                            @php ($full_close = 0)

                            @foreach ($n_model as $key => $val_element)
                            @php ( $full_open = $full_open + $val_element['open'] )
                            @php ( $full_close = $full_close + $val_element['close'] )
                            <tr>
                              <td>{{ $val_element['divisi_name'] }}</td>
                              <td>{{ $val_element['open'] }}</td>
                              <td>{{ $val_element['close'] }}</td>
                              <td>
                                @if ($val_element['total_data'] > 0)
                                  <a href="{{ url('/case_list/indexs') }}?divisi={{ $val_element['divisi_id'] }}">
                                @else
                                  <a href="#">
                                @endif
                                <i class="fa fa-table"></i> Lihat Data</a></td>
                            </tr>
                            @endforeach
                            <tr>
                                <td><b>Total</b></td>
                                <td><b>{{ $full_open }}</b></td>
                                <td><b>{{ $full_close }}</b></td>
                                <td>-</td>
                            </tr>
                          </tbody>
                        </table>

                        @else
                            <h5>Belum ada data</h5>
                        @endif
                    </div>
                    <div class="clear clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .inner-bx-search{
        display: none !important;
        visibility: hidden;
    }
</style>
@endsection
