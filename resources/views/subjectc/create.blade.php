@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Item Pekerjaan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Item Pekerjaan</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">

        <div class="box">
         <div class="box-header">
            <h4>Tambah Item Pekerjaan</h4>
         </div>
         <div class="box-body">

            <form class="form-horizontal" role="form" method="POST" action="{{ url('/backend/admin/subjectc') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label for="title" class="col-md-4 control-label">Nama</label>
                    <div class="col-md-6">
                        <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required="required">
                        @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
         
                <div class="form-group{{ $errors->has('sorting') ? ' has-error' : '' }}">
                    <label for="sorting" class="col-md-4 control-label">Sorting</label>
                    <div class="col-md-6">
                        <input id="sorting" type="number" class="form-control" name="sorting" value="{{ old('sorting') }}" required="required">
                        @if ($errors->has('sorting'))
                        <span class="help-block">
                            <strong>{{ $errors->first('sorting') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
         
         
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-btn fa-user"></i> Simpan
                        </button>
                        <a href="{{ URL::route('subjectc.index') }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Cancel</a>
                    </div>
                </div>
            </form>

         </div>
        </div>

 

     </div>
 </div>
<hr>
 
@endsection