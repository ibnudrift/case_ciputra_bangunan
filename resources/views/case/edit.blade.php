@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Data Case
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Case</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">

        <div class="box">
         <div class="box-header">
            <h4>Update Data Case</h4>
         </div>
         <div class="box-body">

            @if(isset($cases))
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/backend/admin/cases/' . $cases->id) }}" enctype="multipart/form-data">
                <input name="_method" type="hidden" value="PATCH">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('divisi_id') ? ' has-error' : '' }}">
                            <label for="divisi_id" class="col-md-4 control-label">KONTRAKTOR</label>
                            <div class="col-md-8">
                                <select name="divisi_id" id="" class="form-control" required="required">
                                    <option value="0">-- Pilih KONTRAKTOR --</option>
                                    @foreach ($member as $key => $memberon)
                                        @if ($key == old('divisi_id', $cases->divisi_id))
                                        <option selected="selected" value="{{ $key }}">{{ $memberon }}</option>
                                        @else
                                        <option value="{{ $key }}">{{ $memberon }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('divisi_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('divisi_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('proyek_id') ? ' has-error' : '' }}">
                            <label for="proyek_id" class="col-md-4 control-label">SUPERVISOR</label>
                            <div class="col-md-8">
                                <input type="hidden" name="proyek_id" value="1">
                                <select name="users_id" data-user="{{ Auth::user()->id_admin }}" class="form-control" required="required">
                                    <option value="0">-- Pilih Supervisor --</option>
                                    @foreach ($admin as $key => $data_adm)
                                        @if ($key == old('users_id', $cases->users_id))
                                        <option selected="selected" value="{{ $key }}">{{ ucfirst(trans($data_adm)) }}</option>
                                        @else
                                        <option @if ($key == Auth::user()->id_admin) selected="selected" @endif value="{{ $key }}">{{ ucfirst(trans($data_adm)) }}</option>
                                        @endif
                                    @endforeach
                                </select>

                                @if ($errors->has('users_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('users_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">

                        <input type="hidden" name="item_pk_id" value="1">
                        {{-- <div class="form-group{{ $errors->has('item_pk_id') ? ' has-error' : '' }}">
                            <label for="item_pk_id" class="col-md-4 control-label">Item Pekerjaan</label>
                            <div class="col-md-8">
                                <select name="item_pk_id" id="" class="form-control" required="required">
                                    <option value="">-- Pilih Pekerjaan --</option>
                                    @foreach ($subjects as $key => $subjectn)
                                        @if ($key == old('item_pk_id', $cases->item_pk_id))
                                        <option selected="selected" value="{{ $key }}">{{ $subjectn }}</option>
                                        @else
                                        <option value="{{ $key }}">{{ $subjectn }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('item_pk_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('item_pk_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> --}}
                 
                        <div class="form-group{{ $errors->has('no_case') ? ' has-error' : '' }}">
                            <label for="no_case" class="col-md-4 control-label">No Case</label>
                            <div class="col-md-8">
                                <input id="no_case" type="text" class="form-control" name="no_case" value="{{ old('no_case', $cases->no_case) }}" required="required" readonly="readonly">
                                @if ($errors->has('no_case'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('no_case') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        {{-- <div class="form-group{{ $errors->has('kawasan') ? ' has-error' : '' }}">
                            <label for="kawasan" class="col-md-4 control-label">Kawasan / Cluster</label>
                            <div class="col-md-8">
                                <select name="kawasan" id="inp_kawasan" class="form-control" required="required">
                                    <option value="">-- Pilih Kawasan --</option>
                                    @foreach ($kawasan as $key => $kawasanon)
                                        @if ($kawasanon->id == $cases->kawasan)
                                        <option selected="selected" value="{{ $kawasanon->id }}">{{ $kawasanon->title }}</option>
                                        @else
                                        <option value="{{ $kawasanon->id }}">{{ $kawasanon->title }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('kawasan'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('kawasan') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('tipe_rumah') ? ' has-error' : '' }}">
                            <label for="tipe_rumah" class="col-md-4 control-label">Tipe Rumah</label>
                            <div class="col-md-8">
                                <select name="tipe_rumah" class="form-control" id="inp_tipe_rumah" required="required">
                                    <option value="">-- Pilih Tipe --</option>
                                    @foreach ($tipes as $key => $tipeson)
                                        @if ($tipeson->id == $cases->tipe_rumah)
                                        <option selected="selected" value="{{ $tipeson->id }}">{{ $tipeson->nama }}</option>
                                        @else
                                        <option value="{{ $tipeson->id }}">{{ $tipeson->nama }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('tipe_rumah'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('tipe_rumah') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('blok_kavling') ? ' has-error' : '' }}">
                            <label for="blok_kavling" class="col-md-4 control-label">Blok Kavling</label>
                            <div class="col-md-8">
                                <input id="blok_kavling" type="text" class="form-control" name="blok_kavling" value="{{ old('blok_kavling', $cases->blok_kavling) }}" required="required">
                                @if ($errors->has('blok_kavling'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('blok_kavling') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> --}}
                        
                    </div>
                </div>

                <div class="clearfix clear"></div>
                <div class="py-3"></div>
                <div class="clearfix clear"></div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('foto_sebelum') ? ' has-error' : '' }}">
                            <label for="foto_sebelum" class="col-md-4 control-label">Foto Sebelum</label>
                            <div class="col-md-8">
                                <input id="foto_sebelum" type="file" class="form-control" name="foto_sebelum">
                                @if ($errors->has('foto_sebelum'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('foto_sebelum') }}</strong>
                                </span>
                                @endif
                                @if ($cases->foto_sebelum)
                                    <span class="help-block" style="max-width: 200px; margin: 1em 0;">
                                        <a data-fancybox="gallery" href="{{ URL::to('/upload/case/'.$cases->foto_sebelum )}}"><img src="{{ URL::to('/upload/case/'.$cases->foto_sebelum )}}" alt="" class="img-thumbnail img-fluid"></a>
                                    </span>
                                    <input type="hidden" name="save_image1" value="{{ $cases->foto_sebelum }}">
                                @endif
                                <span class="help-block"><small>*) Note: Ukuran file gambar harus kurang dari 2MB, tipe file jpg, png.</small></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('foto_sesudah') ? ' has-error' : '' }}">
                            <label for="foto_sesudah" class="col-md-4 control-label">Foto Sesudah</label>
                            <div class="col-md-8">
                                <input id="foto_sesudah" type="file" class="form-control" name="foto_sesudah">
                                @if ($errors->has('foto_sesudah'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('foto_sesudah') }}</strong>
                                </span>
                                @endif
                                @if ($cases->foto_sesudah)
                                    <span class="help-block" style="max-width: 200px; margin: 1em 0;">
                                        <a data-fancybox="gallery" href="{{ URL::to('/upload/case/'.$cases->foto_sesudah )}}"><img src="{{ URL::to('/upload/case/'.$cases->foto_sesudah )}}" alt="" class="img-thumbnail img-fluid"></a>
                                    </span>
                                    <input type="hidden" name="save_image2" value="{{ $cases->foto_sesudah }}">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        {{-- <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <label for="content" class="col-md-4 control-label">Keterangan Case</label>
                            <div class="col-md-8">
                                <textarea name="content" id="content" class="form-control" rows="2" required="required">{{ old('content', $cases->content) }}</textarea>
                                @if ($errors->has('content'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> --}} 

                        <div class="form-group{{ $errors->has('blok_kavling') ? ' has-error' : '' }}">
                            <label for="blok_kavling" class="col-md-4 control-label">Blok Kavling</label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <select name="blok_v" class="form-control" required="required">
                                            <option value="0">-- Pilih Blok --</option>
                                            @foreach ($blok as $key => $blokon)
                                                @if ($key == old('blok_v', $cases->blok_v))
                                                <option selected="selected" value="{{ $key }}">{{ $blokon }}</option>
                                                @else
                                                <option value="{{ $key }}">{{ $blokon }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        @if ($errors->has('blok_v'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('blok_v') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                    <div class="col-md-1"><span class="less_miring" style="font-size: 19px;">/</span></div>
                                    <div class="col-md-5">
                                        <input id="kavling_v" type="text" class="form-control" name="kavling_v" value="{{ old('kavling_v', $cases->kavling_v) }}" required="required" placeholder="Kavling" autocomplete="off">
                                        @if ($errors->has('kavling_v'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('kavling_v') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div> 

                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('date_selesai') ? ' has-error' : '' }}">
                            <label for="date_selesai" class="col-md-4 control-label">Prakiraan Selesai</label>
                            <div class="col-md-8">
                                <input id="date_selesai" type="text" class="form-control datepicker2" name="date_selesai" value="{{ old('date_selesai', $cases->date_selesai) }}" autocomplete="off">
                                @if ($errors->has('date_selesai'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('date_selesai') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                    </div>
                </div>

                <div class="lines-grey"></div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                            <label for="content" class="col-md-4 control-label">Instruksi</label>
                            <div class="col-md-8">
                                <textarea name="content" id="content" class="form-control" rows="5">{{ old('content', $cases->content) }}</textarea>
                                @if ($errors->has('content'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('content') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> 
                    </div>
                    
                    <div class="col-md-6">
                        {{-- <div class="form-group{{ $errors->has('penyebab') ? ' has-error' : '' }}">
                            <label for="penyebab" class="col-md-4 control-label">Penyebab</label>
                            <div class="col-md-8">
                                <textarea name="penyebab" id="penyebab" class="form-control" rows="3">{{ old('penyebab', $cases->penyebab) }}</textarea>
                                @if ($errors->has('penyebab'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('penyebab') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>  --}}
                        <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>
                            <div class="col-md-8">
                                <select name="status" id="status" class="form-control">
                                    <option @if ($cases->status == 1) selected="selected" @endif value="1">Open</option>
                                    <option @if ($cases->status == 2) selected="selected" @endif value="2">Close</option>
                                </select>
                                @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('is_archive') ? ' has-error' : '' }}">
                                <label for="is_archive" class="col-md-4 control-label">Archive</label>
                                <div class="col-md-8">
                                    <select name="is_archive" id="is_archive" class="form-control">
                                        <option @if ($cases->is_archive == 0) selected="selected" @endif value="0">Tidak di arsip</option>
                                        <option @if ($cases->is_archive == 1) selected="selected" @endif value="1">Arsipkan</option>
                                    </select>
                                    @if ($errors->has('is_archive'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('is_archive') }}</strong>
                                    </span>
                                    @endif
                            </div>
                        </div>                         

                    </div>
                    <div class="col-md-6">

                        {{-- <div class="form-group{{ $errors->has('notes') ? ' has-error' : '' }}">
                            <label for="notes" class="col-md-4 control-label">Notes / Comment</label>
                            <div class="col-md-8">
                                <textarea name="notes" id="notes" class="form-control" rows="3">{{ old('notes', $cases->notes) }}</textarea>
                                @if ($errors->has('notes'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('notes') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> --}} 

                    </div>
                </div>

                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Simpan
                                </button>
                                <a href="{{ URL::route('cases.index') }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @endif

         </div>
        </div>

 

     </div>
 </div>
<hr>
 
 <script type='text/javascript'>

    $(document).ready(function(){
      // Department Change
      $('#sel_depart').change(function(){
         // Department id
         var id = $(this).val();
         // Empty the dropdown
         $('#no_case').empty();

         // AJAX Request
         $.ajax({
               url: '{{ URL::route('customs.nocases') }}?proyek_id='+id,
               type: 'get',
               dataType: 'json',
               success: function(response){
                console.log(response);
                $('#no_case').val(response);
               }
            });

         // Get Kawasan From Proyek
         var Strings_op = '';
         $.ajax({
               url: '{{ URL::route('customs.showkawasan') }}?proyek_id='+id,
               type: 'get',
               dataType: 'json',
               success: function( res ){
                $('#loading').removeClass('hides');

                Strings_op += "<option value=''>-- Pilih Kawasan --</option>";
                $.each(res, function(i, item) {
                   Strings_op += "<option value='" + item.id + "'>" + item.title + "</option>";
                })
                $('#inp_kawasan').html(Strings_op);

                setTimeout(function(){
                    $('#loading').addClass('hides');
                }, 1500);
               }
            });

      });

      // ajax for tipe rumah from kawasan
      $('#inp_kawasan').change(function(){
        var proyek_id = $('#sel_depart').val();
        var kawasan_id = $(this).val();

        var Strings_op2 = '';
         $.ajax({
               url: '{{ URL::route('customs.showtipe') }}?proyek_id='+proyek_id+'&kawasan_id='+kawasan_id,
               type: 'get',
               dataType: 'json',
               success: function( res ){
                // console.log(res);
                $('#loading').removeClass('hides');

                Strings_op2 += "<option value=''>-- Pilih Tipe --</option>";
                $.each(res, function(i, item) {
                   Strings_op2 += "<option value='" + item.id + "'>" + item.nama + "</option>";
                })
                $('#inp_tipe_rumah').html(Strings_op2);

                setTimeout(function(){
                    $('#loading').addClass('hides');
                }, 1500);
               }
            });

      });

    });
    </script>
    <div id="loading" class="blocks_loading hides">
        <div class="inner">
            <div class="logo-loading"></div>
        </div>
    </div>
@endsection