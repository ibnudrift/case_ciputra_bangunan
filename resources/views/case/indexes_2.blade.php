@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Data Listing Case
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Listing Case</li>
      </ol>
    </section>
@endsection
@section('data-content')

<div class="row justify-content-center">
        <div class="col-md-11">

            <a href="#" onclick="window.history.back();" class="btn btn-link"><i class="fa fa-chevron-left"></i> Kembali</a>
            <div class="card">
                <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                      Dashboard  
                    </div>
                    <div class="col-md-6 text-right">
                        <small>PIC: <b>{{ $divisi }}</b></small>
                    </div>
                </div>
                </div>
                <div class="card-body p-5">
                    <div class="lists-shortcut">
                        <div class="table-responsive">
                            @if(count($model) > 0)
                               <table class="table lists_case_m">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Foto Sebelumnya</th>
                                        <th>BLOK / KAVLING</th>
                                        <th>Instruksi</th>
                                        <th>Foto Hasil Perbaikan</th>
                                        <th>Status</th>
                                        <th>-</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($model as $key => $data)
                                    <tr class="{{ $data->status_data }}">
                                      <td>{{ $data->no_case }}</td>
                                      <td>
                                        <div class="well-pic" style="max-width: 80px;"><img class="img-responsive" src="{{ URL::to('/upload/case/'. $data->foto_sebelum )}}" alt=""></div>
                                      </td>
                                      <td>{{ $data->blok_kavling }}</td>
                                      <td>{{ $data->content }}</td>
                                      <td>
                                        <div class="well-pic" style="max-width: 80px;"><img class="img-responsive" src="{{ URL::to('/upload/case/'. $data->foto_sesudah )}}" alt=""></div>
                                      </td>
                                      <td>{{ ($data->status == 1)? "OPEN": "CLOSE" }}</td>
                                      <td><a href="{{ URL::route('case_edit_manager', ['id' => $data->id]) }}" class="btn btn-link"><i class="fa fa-eye"></i></a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                            belum ada data
                            @endif
                            <?php echo $model->render(); ?>
                           <div class="clear clearfix"></div>
                        </div>
                    </div>
                    <div class="clear clearfix"></div>
                </div>
            </div>
        </div>
    </div>

@endsection