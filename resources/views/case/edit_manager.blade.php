@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Data View Case
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data View Case</li>
      </ol>
    </section>
@endsection
@section('data-content')

<div class="pt-0 pl-5 pr-5">
 <div class="row">
     <div class="col-md-12">

        <div class="box">
         <div class="box-header pb-4">
            <h4>Case Detail</h4>
         </div>
         <div class="box-body">

             @if(isset($cases))
            <form class="form-horizontal" role="form" method="post" action="{{ url('/case_list/update') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $cases->id }}">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group frm_admin row {{ $errors->has('divisi_id') ? ' has-error' : '' }}">
                            <label for="divisi_id" class="col-md-4 control-label">PIC</label>
                            <div class="col-md-8">
                                <select name="divisi_id" id="" class="form-control" disabled="disabled">
                                    <option value="0">-- Pilih Kontraktor --</option>
                                    @foreach ($divisi as $key => $division)
                                        @if ($key == old('divisi_id', $cases->divisi_id))
                                        <option selected="selected" value="{{ $key }}">{{ $division }}</option>
                                        @else
                                        <option value="{{ $key }}">{{ $division }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('divisi_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('divisi_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('proyek_id') ? ' has-error' : '' }}">
                            <label for="proyek_id" class="col-md-4 control-label">SUPERVISOR</label>
                            <div class="col-md-8">
                                <input type="hidden" name="proyek_id" value="1">
                                
                                <select name="users_id" data-user="{{ Auth::user()->id_admin }}" class="form-control" disabled="disabled">
                                    <option value="0">-- Pilih Supervisor --</option>
                                    @foreach ($admin as $key => $data_adm)
                                        @if ($key == old('users_id', $cases->users_id))
                                        <option selected="selected" value="{{ $key }}">{{ ucfirst(trans($data_adm)) }}</option>
                                        @endif
                                    @endforeach
                                </select>

                                @if ($errors->has('users_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('users_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">

                        <input type="hidden" name="item_pk_id" value="1">
                        {{-- <div class="form-group row frm_admin {{ $errors->has('item_pk_id') ? ' has-error' : '' }}">
                            <label for="item_pk_id" class="col-md-4 control-label">Item Pekerjaan</label>
                            <div class="col-md-8">
                                <select name="item_pk_id" id="" class="form-control" disabled="disabled">
                                    <option value="0">-- Pilih Pekerjaan --</option>
                                    @foreach ($subjects as $key => $subjectn)
                                        @if ($key == old('item_pk_id', $cases->item_pk_id))
                                        <option selected="selected" value="{{ $key }}">{{ $subjectn }}</option>
                                        @else
                                        <option value="{{ $key }}">{{ $subjectn }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('item_pk_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('item_pk_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> --}}
                 
                        <div class="form-group frm_admin row {{ $errors->has('no_case') ? ' has-error' : '' }}">
                            <label for="no_case" class="col-md-4 control-label">No Case</label>
                            <div class="col-md-8">
                                <input id="no_case" type="text" class="form-control" name="no_case" value="{{ old('no_case', $cases->no_case) }}" required="required" readonly="readonly">
                                @if ($errors->has('no_case'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('no_case') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group frm_admin row {{ $errors->has('status') ? ' has-error' : '' }}">
                            <label for="status" class="col-md-4 control-label">Status</label>
                            <div class="col-md-8">
                                <select name="status" id="status" class="form-control backs_red" disabled="disabled">
                                    <option @if ($cases->status == '1') selected="selected" @endif value="1">Open</option>
                                    <option @if ($cases->status == '2') selected="selected" @endif value="2">Close</option>
                                </select>
                                @if ($errors->has('status'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        {{-- <div class="form-group frm_admin row {{ $errors->has('kawasan') ? ' has-error' : '' }}">
                            <label for="kawasan" class="col-md-4 control-label">Kawasan / Cluster</label>
                            <div class="col-md-8">
                                <select name="kawasan" id="" class="form-control" disabled="disabled">
                                    <option value="0">-- Pilih Kawasan --</option>
                                    @foreach ($kawasan as $key => $kawasanon)
                                        @if ($key == old('kawasan', $cases->kawasan))
                                        <option selected="selected" value="{{ $key }}">{{ $kawasanon }}</option>
                                        @else
                                        <option value="{{ $key }}">{{ $kawasanon }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('kawasan'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('kawasan') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group frm_admin row {{ $errors->has('tipe_rumah') ? ' has-error' : '' }}">
                            <label for="tipe_rumah" class="col-md-4 control-label">Tipe Rumah</label>
                            <div class="col-md-8">
                                <select name="tipe_rumah" id="" class="form-control" disabled="disabled">
                                    <option value="0">-- Pilih Tipe Rumah --</option>
                                    @foreach ($tipes as $key => $tipeson)
                                        @if ($key == old('tipe_rumah', $cases->tipe_rumah))
                                        <option selected="selected" value="{{ $key }}">{{ $tipeson }}</option>
                                        @else
                                        <option value="{{ $key }}">{{ $tipeson }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('tipe_rumah'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('tipe_rumah') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group frm_admin row {{ $errors->has('blok_kavling') ? ' has-error' : '' }}">
                            <label for="blok_kavling" class="col-md-4 control-label">Blok Kavling</label>
                            <div class="col-md-8">
                                <input id="blok_kavling" type="text" class="form-control" name="blok_kavling" value="{{ old('blok_kavling', $cases->blok_kavling) }}" readonly="readonly">
                                @if ($errors->has('blok_kavling'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('blok_kavling') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> --}}
                        
                    </div>
                </div>

                <div class="clearfix clear"></div>
                <div class="py-3"></div>
                <div class="clearfix clear"></div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row {{ $errors->has('foto_sebelum') ? ' has-error' : '' }}">
                            <label for="foto_sebelum" class="col-md-4 control-label">Foto Sebelum</label>
                            <div class="col-md-8">
                                <input id="foto_sebelum" type="file" class="form-control" name="foto_sebelum" disabled="disabled">
                                @if ($errors->has('foto_sebelum'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('foto_sebelum') }}</strong>
                                </span>
                                @endif
                                @if ($cases->foto_sebelum)
                                    <span class="help-block inpictures" style="max-width: 150px; margin: 1em 0 0; display: block;">
                                        <a data-fancybox="gallery" href="{{ URL::to('/upload/case/'.$cases->foto_sebelum )}}">
                                        <img src="{{ URL::to('/upload/case/'.$cases->foto_sebelum )}}" alt="" class="img-thumbnail img img-fluid"></a>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row {{ $errors->has('foto_sesudah') ? ' has-error' : '' }}">
                            <label for="foto_sesudah" class="col-md-4 control-label">Foto Sesudah</label>
                            <div class="col-md-8">
                                <input id="foto_sesudah" type="file" class="form-control hide_gm" name="foto_sesudah">
                                @if ($errors->has('foto_sesudah'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('foto_sesudah') }}</strong>
                                </span>
                                @endif
                                @if ($cases->foto_sesudah)
                                    <span class="help-block inpictures" style="max-width: 150px; margin: 1em 0 0; display: block;">
                                        <a data-fancybox="gallery" href="{{ URL::to('/upload/case/'.$cases->foto_sesudah )}}"><img src="{{ URL::to('/upload/case/'.$cases->foto_sesudah )}}" alt="" class="img-thumbnail img img-fluid"></a>
                                    </span>
                                @endif
                                <span class="help-block"><small style="font-size: 10px;">*) Ukuran gambar kurang dari 2MB, format landscape, tipe file jpg, png.</small></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 frm_admin">
                        <div class="form-group row {{ $errors->has('blok_kavling') ? ' has-error' : '' }}">
                            <label for="blok_kavling" class="col-md-4 control-label">BLOK / KAVLING</label>
                            <div class="col-md-8">
                                <textarea name="blok_kavling" id="blok_kavlings" class="form-control" rows="1" readonly="readonly">{{ old('blok_kavling', $cases->blok_kavling) }}</textarea>
                                @if ($errors->has('blok_kavling'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('blok_kavling') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row {{ $errors->has('date_selesai') ? ' has-error' : '' }}">
                            <label for="date_selesai" class="col-md-4 control-label">Prakiraan Penyelesaian</label>
                            <div class="col-md-8">
                                {{-- datepicker --}}
                                <input type="text" name="date_selesai" id="date_selesai" class="form-control hide_gm" value="{{ old('date_selesai', $cases->date_selesai) }}" readonly="readonly">
                                @if ($errors->has('date_selesai'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('date_selesai') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> 

                        {{-- @if ($cases->date_selesai != '')
                        <div class="form-group row {{ $errors->has('date_reschedule') ? ' has-error' : '' }}">
                            <label for="date_reschedule" class="col-md-3 control-label">Reschedule Penyelesaian</label>
                            <div class="col-md-8">
                                <input type="text" name="date_reschedule" disabled="disabled" id="date_reschedule" class="datepicker form-control" value="{{ old('date_reschedule', $cases->date_reschedule) }}">
                                @if ($errors->has('date_reschedule'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('date_reschedule') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div> 
                        @endif --}}
                    </div>
                </div>

                <div class="lines-grey"></div>
                
                <div class="row">
                    <div class="col-md-12">
                        {{-- <div class="form-group row {{ $errors->has('penyebab') ? ' has-error' : '' }}">
                            <label for="penyebab" class="col-md-3 control-label">Penyebab</label>
                            <div class="col-md-9">
                                <textarea name="penyebab" id="penyebab" class="form-control hide_gm" rows="7">{{ old('penyebab', $cases->penyebab) }}</textarea>
                                @if ($errors->has('penyebab'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('penyebab') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>  --}}

                        <div class="form-group row {{ $errors->has('content') ? ' has-error' : '' }}">
                            <label for="content" class="col-md-2 control-label">Instruksi</label>
                            <div class="col-md-10">
                                    <span class="hide" data-id="{{ Auth::user()->role_member }}"></span>
                                    <textarea name="content" id="content" class="form-control" @if (Auth::user()->role_member != 2 and Auth::user()->role_member != 3) disabled="disabled" @endif rows="7">{{ old('content', $cases->content) }}</textarea>

                                    @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                    @endif
                            </div>
                        </div> 

                        {{-- <div class="form-group frm_admin row {{ $errors->has('notes') ? ' has-error' : '' }}">
                            <label for="notes" class="col-md-3 control-label">Notes / Comment</label>
                            <div class="col-md-9">
                                <div class="pr-5">
                                <textarea name="notes" id="notes" class="form-control" @if (Auth::user()->role_member != 3) disabled="disabled" @endif rows="3">{{ old('notes', $cases->notes) }}</textarea>
                                @if ($errors->has('notes'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('notes') }}</strong>
                                </span>
                                @endif
                                @if (Auth::user()->role_member == 3)
                                    <span class="help-block">
                                        <small style="font-size: 65%;">*) Edit comment bila ada kesalahan komentar dari manager / Kontraktor</small>
                                    </span>
                                @endif
                                </div>
                            </div>
                        </div> --}}

                    </div>
                    <div class="col-md-6">
                        
                    </div>
                </div>
                
                @if ( (Auth::user()->role_member != 4) and Auth::user()->id == $cases->divisi_id )
                <div class="row" data-id="{{ Auth::user()->id }}" data-divisi="{{ $cases->divisi_id }}">
                    <div class="col-md-6">
                        <div class="form-group row mb-0">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Simpan
                                </button>
                                <a href="{{ URL::route('home') }}" class="btn btn-default"><i class="fa fa-btn fa-history"></i> Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

            </form>
            @endif

         </div>
        </div>

 

     </div>
 </div>
<hr style="margin-top: 0px;">

    </div>

<style type="text/css">
    .form_admin .form-control:disabled, 
    .form_admin .form-control[readonly],
    .frm_admin input,
    .frm_admin select,
    .frm_admin textarea
    {
        background-color: #cee6cc !important;
    }
    a.btn,
    button.btn{
        font-size: 0.8rem;
    }
    .inpictures{
        max-width: inherit !important;
    }
    .inpictures img{
        width: 330px;
        height: 160px;
    }
    
</style>
<script type="text/javascript">
    @if (Auth::user()->role_member != 1)
        $(document).ready(function(){
            $('.hide_gm').attr('disabled', 'disabled');
        })
    @endif
</script>
@endsection