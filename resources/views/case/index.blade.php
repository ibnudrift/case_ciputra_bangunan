@extends('layouts.master')
@section('content-header')
<section class="content-header inside">
      <h1>
        Data Case
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Case</li>
      </ol>
    </section>
@endsection
@section('data-content')
 <div class="row">
     <div class="col-xs-12">
        @if (session('notifikasi'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                {{ session('notifikasi') }}
            </div>
        @endif
        <div class="box">
         <div class="box-header">
            <div class="row">
              <div class="col-md-4">
                <h3>Data Case <span class="customs_search">@if ( $filters_divisi ) <small>Kontraktor: {{ $filters_divisi->email }}</small>  @endif</span>
                </h3>
                <a href="{{ URL::route('cases.create') }}" class="btn btn-primary">Tambah Case</a>
              </div>
              <div class="col-md-8">
                <div class="text-right" style="margin-top: 20px;">
                  <form action="{{action('CasesController@index')}}" method="get" role="search">
                    {{-- {{ csrf_field() }} --}}
                      <div class="d-block align-middle">
                          <div class="d-inline-block align-middle pr-2">
                            <label for="">Pilih SUPERVISOR</label>
                          </div>
                          <div class="d-inline-block align-middle pr-2">
                            <select name="spv_id" class="form-control" id="filters_proyekfrm">
                              <option value="">All Supervisor</option>
                              @foreach ($admins as $key => $value)
                                @if ($filter_proyek)
                                  <option @if ($value->id_admin == $filter_proyek->id_admin) selected="selected" @endif  value="{{ $value->id_admin }}">{{ucfirst(trans($value->nama))}}</option>
                                @else
                                  <option value="{{ $value->id_admin }}">{{ucfirst(trans($value->nama))}}</option>
                                @endif
                              @endforeach
                            </select>
                          </div>
                          <div class="d-inline-block align-middle pr-2">
                            <input type="text" class="form-control" name="q" placeholder="Search data" @if($q)value="{{ $q }}"@endif> <span class="input-group-btn">
                            </span>
                          </div>
                          <div class="d-inline-block align-middle">
                            <button type="submit" class="btn btn-default">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                          </div>
                      </div>
                  </form>
                  <div class="clear"></div>
                </div>
              </div>
            </div>
         </div>
         <div class="box-body">

             @if(count($cases) > 0)
                <table class="table lists_case_m">
                    <thead>
                        <tr>
                            <th>Foto Hasil Perbaikan</th>
                            <th>KONTRAKTOR</th>
                            <th>BLOK / KAVLING</th>
                            <th>Status</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($cases as $data)
                        <tr class="{{ $data->status_data }}">
                            <td><img src="{{ URL::to('/upload/case/'.$data->foto_sesudah )}}" alt="" class="img-thumbnail img-fluid" style="max-width: 55px;"></td>
                            <td>{{ $data->nama_divisi }}</td>
                            <td>{{ $data->blok_kavling }}</td>
                            <td>{{ ($data->status == '2')? "CLOSE": "OPEN" }}</td>
                            <td>
                                <a href="{{action('CasesController@show', $data->id)}}" class="btn btn-info"></i> Edit</a>
                                <form action="{{action('CasesController@destroy', $data->id)}}" method="post" class="hapus" style="display:inline">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger to_delete" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
            belum ada data
            @endif
            <?php echo $cases->render(); ?>

            <div class="alert small" role="alert" style="background: #f2f2f2; margin-top: 15px;">
              *) Note:<br>
              Data done progress 100% - warna hijau. <br>
              Case tercancel/ belum selesai - warna merah<br>
              Data case 1-2hari akan selesai - warna orange<br>
              Data case ter progress - warna abu-abu
              <div class="clear"></div>
            </div>

         </div>
         </div>
     </div>
 </div>
<hr>

<script type="text/javascript">
  $(function(){

    $("button.to_delete").click(function() {

        var isGood=confirm('Are You Sure?');
        if (isGood) {
          return true
        } else {
          return false;
        }

    });

  });
</script>
@endsection