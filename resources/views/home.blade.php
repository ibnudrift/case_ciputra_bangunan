@extends('layouts.front')

@section('content-kategori-active')
{{-- <p><i class="icon-sarana"></i> &nbsp;DIVISI PRASARANA</p> --}}
@endsection

@section('content-sidebar-link')
<li>
    <a href="{{ url('case_other') }}"><i class="fas fa-book"></i> &nbsp;Lihat Case Lain</a>
</li>
{{-- <li>
    <a href="#"><i class="fas fa-video"></i> &nbsp;Daftar Isi Video</a>
</li>  --}}
@endsection

@section('content')
<div class="container">

    {{-- <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div> --}}

    <div class="row justify-content-center">
        <div class="col-md-11">
            {{-- start block list --}}
            
            {{-- end block list --}}
            <div class="clear"></div>
        </div>
    </div>    

    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card">
                <div class="card-header">
                <div class="row">
                    <div class="col-md-6">
                      Dashboard  
                    </div>
                    <div class="col-md-6 text-right">
                        <small>PIC: <b>{{ $divisi }}</b></small>
                    </div>
                </div>
                </div>
                <div class="card-body p-5">
                    
                    <div class="lists-shortcut">
                        <div class="table-responsive">
                            @if(count($model) > 0)
                               <table class="table lists_case_m">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Foto Sebelumnya</th>
                                        <th>Blok / Kavling</th>
                                        {{-- <th>Penyebab</th> --}}
                                        <th>Instruksi</th>
                                        <th>Foto Hasil Perbaikan</th>
                                        <th>Status</th>
                                        <th>-</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($model as $key => $data)
                                    <tr class="{{ $data->status_data }}">
                                      <td>{{ $data->no_case }}</td>
                                      <td>
                                        <div class="well-pic" style="max-width: 80px;"><img class="img img-fluid" src="{{ URL::to('/upload/case/'. $data->foto_sebelum )}}" alt=""></div>
                                      </td>
                                      <td>{{ $data->blok_kavling }}</td>
                                      {{-- <td>{{ $data->penyebab }}</td> --}}
                                      <td>{{ $data->content }}</td>
                                      <td>
                                        <div class="well-pic" style="max-width: 80px;"><img class="img img-fluid" src="{{ URL::to('/upload/case/'. $data->foto_sesudah )}}" alt=""></div>
                                      </td>
                                      <td>{{ ($data->status == 2)? "CLOSE": "OPEN" }}</td>
                                      <td><a href="{{ URL::route('case_edit', ['id' => $data->id]) }}" class="btn btn-link"><i class="fa fa-eye"></i></a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                            belum ada data
                            @endif
                            <?php echo $model->render(); ?>
                           <div class="clear clearfix"></div>
                        </div>
                    </div>
                    <div class="clear clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .inner-bx-search{
        display: none !important;
        visibility: hidden;
    }
</style>
@endsection
