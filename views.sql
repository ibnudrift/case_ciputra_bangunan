DROP TABLE IF EXISTS `view_kawasan_tipe`;

CREATE VIEW `view_kawasan_tipe`  AS  
			Select 
			`kawasan`.`id` AS `kawasan_id`,
			`kawasan`.`title` AS `kawasan_nama`,
			`proyek`.`id` AS `proyek_id`,
			`proyek`.`nama` AS `proyek_nama`,
			`proyek`.`info` AS `proyek_info`,
			`tipes`.`id` AS `tipes_id`,
			`tipes`.`nama` AS `tipes_nama`
			From `kawasan` 
			Join `proyek` On(`proyek`.`id` = `kawasan`.`proyek_id`)
			Left Join `tipes` On(`tipes`.`kawasan_id` = `kawasan`.`id`);
